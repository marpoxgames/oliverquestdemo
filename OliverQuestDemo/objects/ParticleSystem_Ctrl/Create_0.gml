 /// @description Insert description here
// You can write your code in this editor



particle_system_weather = part_system_create_layer("weather",0);
particle_system_lights = part_system_create_layer("light",0);

#region Rain
	partycleType_Rain= part_type_create();

	part_type_sprite(partycleType_Rain, sprite_rain_particle, 0, 0 , 1);
	part_type_size(partycleType_Rain,1,1,0,0);
	
	part_type_gravity(partycleType_Rain, 0.05, 250 );
	part_type_life(partycleType_Rain, 50, 100);
	
	part_type_orientation(partycleType_Rain,330,330,0,0,0)
//end region rain
#endregion 



#region Snow Hard



partycleType_SnowHard = part_type_create();

part_type_sprite(partycleType_SnowHard, sprite_snow_particle1,0,0,1);
part_type_size(partycleType_SnowHard, 0.5,1.2,0,0);

part_type_speed(partycleType_SnowHard,0.4,0.6,0,0);
part_type_direction(partycleType_SnowHard, 270, 270, 0 , 15);

part_type_life(partycleType_SnowHard,500,500);
part_type_orientation(partycleType_SnowHard,0,359,0,15,0);
part_type_alpha3(partycleType_SnowHard, 0.8, 0.7, 0.1);



partycleType_SnowCloudHard = part_type_create();
part_type_shape(partycleType_SnowCloudHard , pt_shape_cloud);
part_type_size(partycleType_SnowCloudHard , 1,2,0.01,0);
part_type_orientation(partycleType_SnowCloudHard ,0,359,0,2,0);
part_type_life(partycleType_SnowCloudHard ,200,350);
part_type_blend(partycleType_SnowCloudHard ,1);
part_type_alpha3(partycleType_SnowCloudHard , 0.001, 0.05, 0.001);



#endregion


#region Snow Slow



partycleType_SnowSlow = part_type_create();

part_type_sprite(partycleType_SnowSlow, sprite_snow_particle1,0,0,1);
part_type_size(partycleType_SnowSlow, 0.5,1.2,0,0);

part_type_speed(partycleType_SnowSlow,0.4,0.6,0,0);
part_type_direction(partycleType_SnowSlow, 270, 270, 0 , 15);

part_type_life(partycleType_SnowSlow,500,500);
part_type_orientation(partycleType_SnowSlow,0,359,0,15,0);
part_type_alpha3(partycleType_SnowSlow, 0.8, 0.7, 0.1);



partycleType_SnowCloudSlow = part_type_create();
part_type_shape(partycleType_SnowCloudSlow , pt_shape_cloud);
part_type_size(partycleType_SnowCloudSlow , 1,2,0.01,0);
part_type_orientation(partycleType_SnowCloudSlow ,0,359,0,2,0);
part_type_life(partycleType_SnowCloudSlow ,180,280);
part_type_blend(partycleType_SnowCloudSlow,1);
part_type_alpha3(partycleType_SnowCloudSlow , 0.001, 0.05, 0.001);



#endregion

#region  Fog





partycleType_Fog = part_type_create();
part_type_shape(partycleType_Fog , pt_shape_cloud);
part_type_size(partycleType_Fog , 1,2,0.01,0);
part_type_orientation(partycleType_Fog ,0,359,0,2,0);
part_type_life(partycleType_Fog ,100,200);
part_type_blend(partycleType_Fog,1);
part_type_alpha3(partycleType_Fog , 0.001, 0.05, 0.001);



#endregion





#region  Pollen

partycleType_Pollen = part_type_create();

part_type_sprite(partycleType_Pollen, sprite_pollen_particle , 0 , 0 , 1 );
part_type_size(partycleType_Pollen , 0.5 , 1.5 , 0 , 0 );


part_type_speed(partycleType_Pollen, 0.2 , 0.1 , 0 , 0 );
part_type_direction(partycleType_Pollen, 70 , 110 , 0 , 0 );

part_type_orientation(partycleType_Pollen ,0,359,0,2,0);
part_type_life(partycleType_Pollen ,500,500);
part_type_alpha3(partycleType_Pollen , 0.8, 0.6, 0.1);

#endregion




#region  FireAsh

partycleType_FireAsh = part_type_create();

part_type_sprite(partycleType_FireAsh, sprite_fire_ash_particle, 0 , 0 , 1 );
part_type_size(partycleType_FireAsh , 0.5 , 1.5 , 0 , 0 );


part_type_speed(partycleType_FireAsh, 0.2 , 0.1 , 0 , 0 );
part_type_direction(partycleType_FireAsh, 70 , 110 , 0 , 0 );

part_type_orientation(partycleType_FireAsh ,0,359,0,2,0);
part_type_life(partycleType_FireAsh ,200,200);
part_type_alpha3(partycleType_FireAsh , 0.8, 0.6, 0.1);

#endregion


#region  FireAshBue

partycleType_FireAshBlue = part_type_create();

part_type_sprite(partycleType_FireAshBlue, sprite_fire_ashblue_particle, 0 , 0 , 1 );
part_type_size(partycleType_FireAshBlue , 0.5 , 1.5 , 0 , 0 );


part_type_speed(partycleType_FireAshBlue, 0.2 , 0.1 , 0 , 0 );
part_type_direction(partycleType_FireAshBlue, 70 , 110 , 0 , 0 );

part_type_orientation(partycleType_FireAshBlue ,0,359,0,2,0);
part_type_life(partycleType_FireAshBlue ,200,200);
part_type_alpha3(partycleType_FireAshBlue , 0.8, 0.6, 0.1);



#endregion



#region water source
	partycleType_WaterSource= part_type_create();
	part_type_sprite(partycleType_WaterSource, sprite_waterSource_particle, 0, 0 , 1);
	part_type_size(partycleType_WaterSource,1,1,0,0);
	part_type_alpha3(partycleType_WaterSource , 0.9, 0.7, 0.5);
	
	part_type_direction(partycleType_WaterSource, 45 , 135 , 0 , 0);
	part_type_gravity(partycleType_WaterSource, 0.3 , 270 );
	
	part_type_gravity(partycleType_WaterSource, 0.3, 270 );
	part_type_life(partycleType_WaterSource, 25, 30);
	part_type_speed(partycleType_WaterSource, 2.0, 4.0 , -0.05, 0);
	
	
	


#endregion


#region water Splash
	partycleType_WaterSplash= part_type_create();
	part_type_sprite(partycleType_WaterSplash, sprite_rain_splash, 0, 0 , 1);

	part_type_alpha3(partycleType_WaterSplash , 0.9, 0.7, 0.5);
	part_type_life(partycleType_WaterSplash,5,10);

	
#endregion