/// @description Insert description here
// You can write your code in this editor
/// @description Inserte aquí la descripción
// Puede escribir su código en este editor

// Inherit the parent event
event_inherited();



intDir=1;
intM= 1.0 ;//Multiplicador //*global.bolPause

intVYMax = 9 * intM; // GRAVEDAD  
intVXMax= 2.5 * intM; //LIMITE VELOCIDAD

intJumpHeight = 8 * intM; //SALTO
intGravityNorm= 0.3* intM; //INCREMENTO DE LA GRAVEDAD-FRICCION 

intGroundAcc= 1.0 *intM; // ACELERACION CORRIENDO 
intAirAcc = 0.4 * intM; // ACELERACION EN EL AIRE

//---------VARIABLES PROB.CHANGE STATE----------------------
intProbChangeDirection=2;
intProbChangeStateMove=4;
intProbChangeStateIdle=0.8;
intProbChangeSprite=1.5;
//-----------------SPRITES----------------------------
spriteNpcIdlePrincipal=spr_npc_priest_idle;
speedSpriteIdlePrincipal=1;

spriteNpcIdleSecond=spr_npc_priest_idle;
speedSpriteIdleSecond=1;

spriteNpcIdleThird=spr_npc_priest_idle;
speedSpriteIdleThird=1;
//--------------
spriteNpcMove=spr_npc_priest_idle;
speedSpriteMove=1;
//-----------------------
spriteNpcJumpUp=spr_npc_priest_idle;
speedSpriteJumpUp=1;

spriteNpcJumpDown=spr_npc_priest_idle;
speedSpriteJumpDown=1;