/// @description Insert description here
// You can write your code in this editor
/// @description Inserte aquí la descripción
// Puede escribir su código en este editor

// Inherit the parent event
event_inherited();



intDir=1;
intM= 1.0 ;//Multiplicador //*global.bolPause

intVYMax = 9 * intM; // GRAVEDAD  
intVXMax= 2.5 * intM; //LIMITE VELOCIDAD

intJumpHeight = 8 * intM; //SALTO
intGravityNorm= 0.3* intM; //INCREMENTO DE LA GRAVEDAD-FRICCION 

intGroundAcc= 1.0 *intM; // ACELERACION CORRIENDO 
intAirAcc = 0.4 * intM; // ACELERACION EN EL AIRE

//---------VARIABLES PROB.CHANGE STATE----------------------
intProbChangeDirection=2;
intProbChangeStateMove=4;
intProbChangeStateIdle=0.8;
intProbChangeSprite=1.5;
//-----------------SPRITES----------------------------
spriteNpcIdlePrincipal=sprite_pavo_idle;
speedSpriteIdlePrincipal=1;

spriteNpcIdleSecond=sprite_pavo_idle_parpadeando;
speedSpriteIdleSecond=1;

spriteNpcIdleThird=sprite_pavo_idle_parpadeando;
speedSpriteIdleThird=1;
//--------------
spriteNpcMove=sprite_pavo_walking;
speedSpriteMove=1;
//-----------------------
spriteNpcJumpUp=sprite_pavo_idle;
speedSpriteJumpUp=1;

spriteNpcJumpDown=sprite_pavo_idle;
speedSpriteJumpDown=1;