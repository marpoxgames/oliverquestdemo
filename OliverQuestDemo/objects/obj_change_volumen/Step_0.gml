/// @ACTUALIZA EL SPRITE SEGUN EL VOLUMEN
 // Inherit the parent event
event_inherited();
//---------------------------------
if(boolActive){
	scr_sound_volumen_config(typeVolumen,intVolumen);
	scr_sound_volumen_saves(typeVolumen);
	
	boolActive=false;
}
image_speed=0;


switch(intVolumen){
case 0:
image_index=0;
break;
case 0.2:
image_index=1;
break;
case 0.4:
image_index=2;
break;
case 0.6:
image_index=3;
break;
case 0.8:
image_index=4;
break;
case 1:
image_index=5;
break;
}  



switch(typeVolumen)
{
	case "music":
	intVolumen=global.volumenSoundMusic;               
	break;
	case "fxs":
	intVolumen=global.volumenSoundFXS;
	break;

	case "timer":
	intVolumen=global.volumenSoundTimer;
	break;
	
}