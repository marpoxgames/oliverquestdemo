           /// @inicial variables globales
showDebugGame=false;
showDebugGlobals=false;
//---CREA OTROS OBJETOS CONTROL-----

instance_create_depth(x,y,-1,obj_transition_cartoon_circle_In);
instance_create_layer(0,0,"control",obj_locale_manager);
instance_create_layer(0,0,"control",obj_camera);
instance_create_layer(0,0,"control",obj_GUI_control);
instance_create_layer(0,0,"control",ParticleSystem_Ctrl);

//id de checkpoint para rspawn
//global.playerSpawnX=0;
//global.playerSpawnY=0;
//VarPausa

//global.bolPause=1;//TODO

//-------------------------------------------------------

//----CONTROL DE GUI STARTMENU---------------------------------------------------
 global.boolStartGame=false;
 boolShowStart=false;
 intAlphaStart=0.001;
 intAlphaPressKey=0;
 upAlphaPressKey=false;
 intAlphaInstruction=0;
 boolAlphaInstruction=false;
 //-----Cargar el juego --------
 global.boolLoadGame=false;
//----OPCIONES--------
// -0- start_menu - Menu de Inicio 
// -1- game_new - Nuevo Juego 
// -2- game_load - Cargar Juego
// -3- game-opcion - Opciones 
//-----------------------PLAYER DATA-----------------------------------
//-------------------------------------------------------------------
lastDoorDefault=-1;
roomDefault=room_town_demo;
coordXPlayerDefault=2142;
coordYPlayerDefault=384;
ini_open("game_data.ini");
global.lastDoor=ini_read_string("player-data","lastDoor",lastDoorDefault);
global.roomActual=ini_read_string("player-data","roomActual",roomDefault);
global.coordXPlayer=ini_read_real("player-data","coordXPlayer",coordXPlayerDefault);
global.coordYPlayer=ini_read_real("player-data","coordYPlayer",coordYPlayerDefault);
ini_close();
global.valueTimerGlobalSec=0;
global.valueTimerGlobalMin=0;
global.valueTimerGlobalHrs=0;
global.valueTimerGlobalDays=0;

//Fuente de Sprites - TODOOO 

//------------------------SEETINGS-----------------------------------
//-------------------------------------------------------------------

//----------------------  RESOLUTION SETTINGS ------------------------
widthDefault=1024;
heightDefault=768;
boolFullScreenDefault=false;
ini_open("game_settings.ini");
global.displayFull=ini_read_string("settings-resolution","full-screen",boolFullScreenDefault);
global.displayWidth=ini_read_real("settings-resolution","width",widthDefault);
global.displayHeight=ini_read_real("settings-resolution","height",heightDefault);
ini_close();
scr_resolution_config();
//----------------------------------------------------------------------

//----------------------------------------------------------------------


 
//---------------------SOUND SETTINGS ----------------------------------
//-----------------------------------------------------------------------
volumenSoundMusicDefault=0.6;
volumenSoundFXSDefault=0.8;
volumenSoundTimerDefault=1;
ini_open("game_settings.ini");
global.volumenSoundMusic=ini_read_real("settings-sound","soundMusic",volumenSoundMusicDefault);
global.volumenSoundFXS=ini_read_real("settings-sound","soundFXS",volumenSoundFXSDefault);
global.volumenSoundTimer=ini_read_real("settings-sound","soundTimer",volumenSoundTimerDefault);
ini_close();  
 
sound=sound_fondo1;
audio_play_sound(sound,100,true);
//----------------------------------------------------------------------


 
//---------------------CONTROLS SETTINGS ----------------------------------
//-----------------------------------------------------------------------


controlPunchDefault=ord("D")
controlJumpDefault=ord("S");
controlActionDefault=ord("E");

controlLeftDefault=vk_left;
controlRightDefault=vk_right;
controlDownDefault=vk_down;
controlUpDefault=vk_up;                                                       

controlPauseDefault=vk_escape;
 
ini_open("game_settings.ini");
global.controlPunch=ini_read_string("settings-control","controlPunch",controlPunchDefault);
global.controlJump=ini_read_string("settings-control","controlJump",controlJumpDefault);
global.controlAction=ini_read_string("settings-control","controlAction",controlActionDefault);
global.controlLeft=ini_read_string("settings-control","controlLeft",controlLeftDefault);
global.controlRight=ini_read_string("settings-control","controlRight",controlRightDefault);
global.controlDown=ini_read_string("settings-control","controlDown",controlDownDefault);
global.controlUp=ini_read_string("settings-control","controlUp",controlUpDefault);
global.controlPause=ini_read_string("settings-control","controlPause",controlPauseDefault);
ini_close(); 
 
   


