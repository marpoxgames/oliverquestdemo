    
// Puede escribir su código en este editor
 

 var anchoGUI=display_get_gui_width();
 var altoGUI=display_get_gui_height();
 draw_set_font(font_textBox_Pixel1);
 draw_set_color(c_black);
 draw_set_alpha(1);
 
 
 
if(!global.boolStartGame){
	//SET GUI SIZE FOR LOGO
		display_set_gui_size(global.anchoCamara*1.3,global.altoCamara*1.3);
	//DRAW IN PRE-START GAME 
		draw_set_alpha(intAlphaStart);
		draw_sprite(sprite_logo_menu_start_v1,0,160,80+(intAlphaPressKey*5));
		
		draw_set_alpha(intAlphaPressKey);
		draw_text(160,140,yd_lang("pressAnyKeyToStart"));
		
}else{
	//SET GUI SIZE FOR GAME
	display_set_gui_size(global.anchoCamara*2,global.altoCamara*2);
	
	//DRAW UN GAME ---
	draw_set_font(global.fontGUI);
	draw_set_alpha(1);
	draw_set_color(c_white);
	if( room_get_name(room)=="room_main_menu" ){
		if(instance_exists(obj_player)){
	
	
			if(  obj_player.intMove==0  ){
				boolAlphaInstruction=true;
			}else{
				boolAlphaInstruction=false;
			}
			
			// TO DO DRAW INSTRUCCION
			draw_set_alpha(intAlphaInstruction);
			script_draw_instructions(intAlphaInstruction);
	 
		}
	}
}

 
 draw_set_alpha(1);
if(showDebugGlobals){
	 draw_set_font(font_textBox_Pixel1);
	 draw_set_color(c_black);
	 draw_set_alpha(1);
	 
		draw_text(0,0,"GLOBALS DEBUGS")
		
		//---DISPLAY---
		draw_text(0,20,"DISPLAYS=")
		draw_text(0,30,"display (almacenado) size:"+string(global.displayWidth)+"  "+string(global.displayHeight));  
	    draw_text(0,40,"Camera size:"+string(global.anchoCamara)+"  "+string(global.altoCamara)); 
	    draw_text(0,50,"boolFullScreen:"+string(global.displayFull));

		//-----UBICACION----
		draw_text(0,70,"UBICACION=")
		draw_text(0,80,"Last Door:"+string(global.lastDoor));
		draw_text(0,90,"room Actual:"+string(global.roomActual));
		draw_text(0,100,"coord X Player:"+string(global.coordXPlayer));
		draw_text(0,110,"coord Y Player:"+string(global.coordYPlayer));
		
		//------CONTROLES-----------------------
		draw_text(anchoGUI/3,0,"CONTROLS=")
		draw_text(anchoGUI/3,10,"control Action:"+string(chr(global.controlAction)));
		draw_text(anchoGUI/3,20,"control Down:"+string(chr(global.controlDown)));
		draw_text(anchoGUI/3,30,"control Jump:"+string(chr(global.controlJump)));
		draw_text(anchoGUI/3,40,"control Left:"+string(chr(global.controlLeft)));
		draw_text(anchoGUI/3,50,"control Pause:"+string(chr(global.controlPause)));
		draw_text(anchoGUI/3,60,"control Punch:"+string(chr(global.controlPunch)));
		draw_text(anchoGUI/3,70,"control Right:"+string(chr(global.controlRight)));
		draw_text(anchoGUI/3,80,"control Up:"+string(chr(global.controlUp)));
		
		
		//---- DATA GAME----------------
		draw_text((anchoGUI/3)*2,0,"DATA GAME");
		draw_text((anchoGUI/3)*2,10,"boolLoadGame:"+string(global.boolLoadGame));
		draw_text((anchoGUI/3)*2,20,"fontGUI:"+string(global.fontGUI));
		draw_text((anchoGUI/3)*2,30,"Game Language:"+string(global.game_language));
		draw_text((anchoGUI/3)*2,40,"yd_lang_idx:"+string(global.yd_lang_idx));
		draw_text((anchoGUI/3)*2,50,"yd_languajes:"+string(global.yd_languajes));
		draw_text((anchoGUI/3)*2,60,"yd_locale_map:"+string(global.yd_locale_map));
		draw_text((anchoGUI/3)*2,70,"yd_locale_words:"+string(global.yd_locale_words));

} 
 
if(showDebugGame){
	draw_set_font(Text_test);
	draw_set_color(c_black);
	 draw_set_alpha(1);
	    
  
  
        draw_text(0,0,"fps :" + string(fps)+"     fps real:   " + string(fps_real));
        draw_text(0,30,"GUI size:"+string(display_get_gui_width())+"  "+string(display_get_gui_height()));
	    draw_text(0,40,"DISPLAY(pantalla) size:"+string(display_get_width())+"  "+string(display_get_height()));
	    draw_text(0,50,"windows(ventana) size:"+string(window_get_width())+" x "+ string(window_get_height()));
    	draw_text(0,60,"window scala:"+string(window_get_width()/window_get_height()));
		
		if(instance_exists(obj_player)){			
		draw_text(anchoGUI/3,10,"PLAYER intActualQuest:" + string(obj_player.intActualQuest));
		draw_text(anchoGUI/3,20,"PLAYER valueAnswer:" + string(obj_player.valueAnswer));
		draw_text(anchoGUI/3,30,"PLAYER VY:" + string(obj_player.intVY));
		draw_text(anchoGUI/3,40,"PLAYER VX:" + string(obj_player.intVX));
		draw_text(anchoGUI/3,50,"PLAYER vspeed:" + string(obj_player.vspeed));
		draw_text(anchoGUI/3,60,"PLAYER hspeed:" + string(obj_player.hspeed));
		draw_text(anchoGUI/3,70,"PLAYER depth:" + string(obj_player.depth));
		draw_text(anchoGUI/3,80,"PLAYER room:" + room_get_name(room)+" -#- "+string(room));
		draw_text(anchoGUI/3,90,"PLAYER TIME GLOBAL:" + string(global.valueTimerGlobalHrs )+":" +string(global.valueTimerGlobalMin )+":" +string(global.valueTimerGlobalSec ) + " days:"+string(global.valueTimerGlobalDays));
	//	draw_text(anchoGUI/3,100,"PLAYER TIME GLOBAL:" + );
		
		}
  		if(instance_exists(obj_lights)){
			
			draw_text(anchoGUI/3,100,"CycleDay:" + obj_lights.dayCycle + " " + string(obj_lights.time));
		}
		
		
		if(instance_exists(obj_kit)){
			//TODO organizar mejor variables para debug sabio 
		draw_text((anchoGUI/3)*2,10,"PLAYER OFENDIDO ACCUM:" + string(obj_kit.AccumOfendido));
		draw_text((anchoGUI/3)*2,20,"KIT valueAnswer:" + string(obj_kit.valueAnswer));
		draw_text((anchoGUI/3)*2,30,"KIT AccumRightArray[0]:" + string(obj_kit.AccumRightArrayTotal));
		draw_text((anchoGUI/3)*2,40,"KIT nivelQuest:" + string(obj_kit.intNivelQuest));
		draw_text((anchoGUI/3)*2,50,"KIT fail:" + string(obj_kit.AccumFail));
		draw_text((anchoGUI/3)*2,60,"KIT query:" + string(obj_kit.query));
		draw_text((anchoGUI/3)*2,70,"KIT right:" + string(obj_kit.AccumTotalRight));
		draw_text((anchoGUI/3)*2,80,"TIMER:" + string(ceil(obj_kit.valueTimer)));
		draw_text((anchoGUI/3)*2,90,"KIT STATE:" + obj_kit.sabio_state);
		}
		//TODO DEBUG OBJETOS ANSWER SABIO DEMO
		
		
		if(instance_exists(obj_chicken)){
			draw_text(0,altoGUI/2,"npc state pavo:"+obj_chicken.npc_state);
		}
		if(instance_exists(npc_familyDad)){
			draw_text(0,altoGUI/2+10,"npc state dad:"+npc_familyDad.npc_state);
		}
		if(instance_exists(npc_familyMom)){
			draw_text(0,altoGUI/2+20,"npc state mom:"+npc_familyMom.npc_state);
		}
		if(instance_exists(npc_familyDaugther)){
			draw_text(0,altoGUI/2+30,"npc state daugther:"+npc_familyDaugther.npc_state);
		}
		if(instance_exists(npc_priest)){
			draw_text(0,altoGUI/2+40,"npc state priest:"+npc_priest.npc_state);
		}
		/*if(instance_exists(npc_shopper)){
			draw_text(0,altoGUI/2+50,"npc state shopper:"+npc_shopper.npc_state);
		}*/
		if(instance_exists(npc_musician)){
			draw_text(0,altoGUI/2+60,"npc state musician:"+npc_musician.npc_state);
		}
		if(instance_exists(npc_librarian)){
			draw_text(0,altoGUI/2+70,"npc state librarian:"+npc_librarian.npc_state);
		}
		
		

}  

