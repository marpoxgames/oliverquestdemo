 /// @description Inserte aquí la descripción
// Puede escribir su código en este editor

// Inherit the parent event
event_inherited();

//----------ASIGNACION DIALOGOS---(change with sabio----------------------------
//----asigna nombre valor de busqueda
categoria="Categoria";

objAnswer0=object_PunchFungi_A;
objAnswer1=object_PunchFungi_B;
objAnswer2=object_PunchFungi_C;
objAnswer3=object_PunchFungi_D;
//------------------ANIMACION----------------
//--Transformation---------------------------
sabioSpriteTransformation=sprite_puff_appearance;
//-----------------IDLE--------------------------
sabioSpriteIdleNormal=sprite_npc_kit_normal_idle;
sabioSpriteIdleStay=sprite_npc_kit_stay_idle;
//--------------------TALK-----------------------
sabioSpriteIdleNormalTalk=sprite_npc_kit_normal_talk;
sabioSpriteIdleStayTalk=sprite_npc_kit_stay_talk;
//----------------------QUEST-------------------------
sabioSpriteQuestNormal=sprite_npc_kit_normal_quest;
sabioSpriteQuestStay=sprite_npc_kit_stay_quest;
//----------------------COMPLETE/DEAD---------------------
sabioSpriteComplete=sprite_npc_kit_complete;
sabioSpriteDead=sprite_npc_kit_dead;
 //---------------TEXT BOXT SABIO---------------
spriteTextBoxSabio=sprite_msm_sabioDemo;


