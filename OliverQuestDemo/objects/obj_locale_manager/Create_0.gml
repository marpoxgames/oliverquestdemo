    /// @description Inserte aquí la descripción
// Puede escribir su código en este editor

global.yd_languajes = [];
global.yd_lang_idx=0;
global.yd_locale_map= ds_map_create();
global.yd_locale_words=0;

 
var default_locale_code="es";
var default_locale_idx = 0;


//BUSCA EL IDIOMA GUARDADO , SINO USA EL POR DEFECTO
languageDefault=os_get_language();
ini_open("game_settings.ini");
global.game_language=ini_read_string("settings-language","language",languageDefault);
ini_close();
//
var native_locale_code=global.game_language;
var native_locale_idx = 0;

//find the first file matching our naming convention 

var locale_file_name= file_find_first("*.json",0);
if( locale_file_name == "" )
{
	show_error("no locale file found ",true);
	return;
}
while(locale_file_name !="")
{
		show_debug_message("loading locale file:"+ locale_file_name);
		
	// open the locale file for reading 
	var locale_file = file_text_open_read(locale_file_name);
	if(locale_file == -1)
	{
		show_error("LOCALE WARNING: ERROR READING LOCALE FILE"+locale_file_name,true);
		return;
	}
	
	//build json text
	
	var json_str ="";
	var j=0;
	while(!file_text_eof(locale_file))
	{
		json_str += file_text_read_string(locale_file);
		file_text_readln(locale_file);
		j++;
	}
	file_text_close(locale_file);
	show_debug_message("loaded locale_map from "+ string(j) + "lines of text");
	 
	//convert json to a usable ds_map
	
	var locale_map = json_decode(json_str);
	
	if(locale_map == -1 || !ds_map_exists(locale_map, "locale_code"))
	{
		show_error("LOCALE WARNING: INVALID LOCALE DATA INSIDE :"+locale_file_name,true);
		return;
		 
	}
	
// get locale code from locale date

var locale_code= locale_map[? "locale_code"];
ds_map_add(global.yd_locale_map,locale_code,locale_map);

// if this is the native local, set the native local index 
if(locale_code == native_locale_code)
{
	native_locale_idx=global.yd_lang_idx;
}else if(locale_code==default_locale_code) 
{
	default_locale_idx=global.yd_lang_idx;
}
//add this locales code to the list of the available locales

global.yd_languajes[global.yd_lang_idx]=locale_code;
global.yd_lang_idx++;

locale_file_name= file_find_next();


}

file_find_close();



// set the default locale to the players native locale if possible 
if(ds_map_exists(global.yd_locale_map, native_locale_code))
{
	show_debug_message("setting locale to native lenguaje ");
	global.yd_locale_words = global.yd_locale_map[? native_locale_code];
	global.yd_lang_idx=native_locale_idx;
	
}else if (ds_map_exists(global.yd_locale_map, default_locale_code))
{
	show_debug_message("setting locale to default lenguaje ");
	global.yd_locale_words = global.yd_locale_map[? default_locale_code];
	global.yd_lang_idx=default_locale_idx;
	
}else
{
	show_debug_message("setting locale to fallback lenguaje:");
	global.yd_lang_idx=0;
	global.yd_locale_words=global.yd_locale_map[? global.yd_languajes[0]];
}

//LENGUAJE ACTUAL
show_debug_message( global.yd_languajes[global.yd_lang_idx]);

