 
//------------------------------------------------
//--------------ESTADOS POR DEFECTO---------------
npc_state="idle";
boolAnimation=false;
image_speed= 0.1;
depth=-1;
//-----------VARIABLES DE MOVIMIENTO--------
intVY=0;// VELOCIDAD VERTICAL
intVX=0;// VELOCIDAD HORIZONTAL

intDir=1;
intM= 1.0 ;//Multiplicador //*global.bolPause

intVYMax = 9 * intM; // GRAVEDAD  
intVXMax= 2.5 * intM; //LIMITE VELOCIDAD

intJumpHeight = 8 * intM; //SALTO
intGravityNorm= 0.5* intM; //INCREMENTO DE LA GRAVEDAD-FRICCION 

intGroundAcc= 1.0 *intM; // ACELERACION CORRIENDO 
intAirAcc = 0.75 * intM; // ACELERACION EN EL AIRE

//--------------VAR.TO CHANGE-------------------
npcName="npcName";
//---------VARIABLES PROB.CHANGE STATE----------------------
intProbChangeDirection=2;
intProbChangeStateMove=3;
intProbChangeStateIdle=0.25;
intProbChangeSprite=1.5;
//-----------------SPRITES----------------------------
spriteNpcIdlePrincipal=-1;
speedSpriteIdlePrincipal=1;

spriteNpcIdleSecond=-1;
speedSpriteIdleSecond=1;

spriteNpcIdleThird=-1;
speedSpriteIdleThird=1;
//--------------
spriteNpcMove=-1;
speedSpriteMove=1;
//-----------------------
spriteNpcJumpUp=-1;
speedSpriteJumpUp=1;

spriteNpcJumpDown=-1;
speedSpriteJumpDown=1;

//--------DIALOGOS ------
//-----------------------
intInteraction=0;



pagina = 0; // contador de pagina actual
//------PRIMER DIALOGO POR DEFECTO----------------------
paginas[0]= 0;

mostrarVentanaNPC=false; //mostrar cartel por defecto 
pagina= -1;    
 //MUESTRA EL MODAL DE LA QUEST 
 
 //-------------LOAD DATA--------------------
 script_load_npc();
 
 
 
 //SHADOWWW
 
 
 shadow = instance_create_depth(x,y,depth-1,obj_shadow_follow);
 shadow.follow= object_index;
