  				 /// @INTERACCION PLAYER
var keyAction=keyboard_check_pressed(global.controlAction);
	 //----------------------------------------------------------------------------
	 //----------------------INTERACCION 0 CON EL PLAYER--------------------------
	 //---------------------------------------------------------------------------
 //---------------------------------INTERACION CONTACTO---------------------------------
/* if(keyAction  && distance_to_object(obj_player)<32){
	 mostrarVentanaNPC =true; //MUESTRA EL MODAL DE LA QUEST 
 }*/
 
 //---------------TO CHANGE DIALOG in NPCS
 switch(intInteraction){
	 case 0:
	    paginas[0]=yd_lang(npcName+"-dial0-"+string(intInteraction));
	    paginas[1]=yd_lang(npcName+"-dial1-"+string(intInteraction));
		paginas[2]=yd_lang(npcName+"-dial2-"+string(intInteraction));
	 break;
	 
	
	case 1:
	    paginas[0]=yd_lang(npcName+"-dial0-"+string(intInteraction));
	 break; 
	 case 2:
	    paginas[0]=yd_lang(npcName+"-dial0-"+string(intInteraction));
		paginas[1]=yd_lang(npcName+"-dial1-"+string(intInteraction));
	 break; 
	 /*
	 .......TODO MORE*/
 }
 
 
 if(keyAction  && distance_to_object(obj_player)<32  &&  !obj_player.boolInDialog){
	 obj_player.boolInDialog=true;
	 mostrarVentanaNPC =true; //MUESTRA EL MODAL DE LA QUEST 
		npc_state="idle";
 }
 
 
 
 //----------------------------------------------------------------------------------------------------------------
//------------------------------------------------CUADRO DE DIALOGO -----------------------------------------------
//----------------------------------------------------------------------------------------------------------------
//------  CIERRA EL MODAL--X DISTANCIA
 if (distance_to_object(obj_player)>32  && mostrarVentanaNPC  ){
		mostrarVentanaNPC =false;
		pagina=-1; 
		obj_player.boolInDialog=false;
		
 }
 //--------CIERRA EL MODAL----X LIMITE DE PAGINAS
 if(keyAction && mostrarVentanaNPC  ){

    if(pagina+1<array_length_1d(paginas)){
	 pagina++;
	 }else{
	 //Si se pasa el maximo de paginas reincia el cartel 
	 mostrarVentanaNPC =false;
	 pagina=-1;
	 obj_player.boolInDialog=false;
	 

//--------------CHANGE DINAMIC DIALOG-----------solamente al finalizar el texto
	 switch(intInteraction){
	 case 0:
	 //Resetea las paginas
		paginas=0;
	//Asigna la interaccion siguiente
		 intInteraction=1;
		script_saveInteraction_npc(npcName,intInteraction);
		
	 break;
	 }
	 
 }   
 }
 
//----SCRIPT COMPRUEBA SI HAY SUELO ABAJO-----------///
bolGround   = scr_ground();//Script comprueba el suelo 
//----------------GRAVEDAD----------------
if(!bolGround)
{
	intVY = scr_approach(intVY, intVYMax, intGravityNorm);//Caida libre
}
//Definir Aceleracin y Friccion en funcion del medio
if(!bolGround)
{
	intTempAcc = intAirAcc;   //ACELERACION EN EL AIRE
}else{
	intTempAcc = intGroundAcc; // ACELERACION EN EL SUELO
}
   

//-----------CAMBIO DE ESTADO A JUMP------------
if(!bolGround)
{
	npc_state="jump";
}else{
if(npc_state=="jump"){
	npc_state="idle"
}
}


	
	
//-------------STATE MACHINE ----------------
switch(npc_state){
	case "idle":
	intVX=0;
	

	alarm[2]=room_speed*random(intProbChangeStateMove)+1; //STAR MOVE 

	alarm[3]=room_speed*random(intProbChangeStateMove)+1; //STAR MOVE 
	
	
	break;
	case "secondAction":
	intVX = 0;
	break;
	case "thirdAction":
	intVX = 0;
	break;
	case"move":
	intVX = scr_approach(intVX,intVXMax*intDir,intTempAcc);	
	//if(choose(1,2,3)==1){intDir*=-1;}
	alarm[1]=room_speed*random(intProbChangeStateIdle)+1; //STAR IDLE- STOP MOVE
	break;
	
	case "jump":
	intVX = scr_approach(intVX,intVXMax*intDir,intTempAcc);
	break;
}


if(boolAnimation)
{
	npc_state=	choose("secondAction","thirdAction");
	boolAnimation=false;
}
//-----------------------------------------------------
//----------CHANGE DIRECTION CONTRA PARED--------------
//-----------------------------------------------------
if(!place_free(x+(intDir*35),y-32))
{
	intDir*=-1;
}
//---------STATE MACHINE ANIMATION ------------

//----------IMAGE SEGUN DIRECTION
image_xscale=intDir;

switch(npc_state){
	case "idle":
	sprite_index=spriteNpcIdlePrincipal;
	image_speed=speedSpriteIdlePrincipal;
	break;
	
	case"secondAction":
		sprite_index=spriteNpcIdleSecond;
		image_speed=speedSpriteIdleSecond;
	if(image_index>=image_number-1)
	{npc_state="idle";}
	break;
	
	case"thirdAction":
		sprite_index=spriteNpcIdleThird;
		image_speed=speedSpriteIdleThird;
	if(image_index>=image_number-1)
	{npc_state="idle";}
	break;
	
	case"move":
	sprite_index=spriteNpcMove;
	image_speed=speedSpriteMove;		
	break;
	
	case "jump":
	if(intVY<0){//---- SUBIDA DE SALTO
		sprite_index=spriteNpcJumpUp;
		image_speed=speedSpriteJumpUp;
	}else{// ------------- BAJADA DE SALTO
		sprite_index=spriteNpcJumpDown;
		image_speed=speedSpriteJumpDown;
	}
//-------------
	break;
}

//---------------------------------------------------------------------------------------
//----------------------------------COLISIONES-------------------------------------------
//---------------------------------------------------------------------------------------
script_check_colision(intVX,intVY);