 
event_inherited();
//----------ASIGNACION DIALOGOS---(change with sabio----------------------------
//----asigna nombre valor de busqueda
categoria="Categoria";



//----TO PLAY------
valueAnswerSabio="VA"+categoria;
valueScoreSabio="VS"+categoria;
valueQuerySabio="VQ"+categoria;
valueAnsOpcionSabio="VQO"+categoria;
//----FIRST DIALOG-----
dialogFirst0="PD"+categoria+"0";
dialogFirst1="PD"+categoria+"1"

//---DIALOG FAIL TIMER---

dialogFailTimer0="FT"+categoria+"0";
dialogFailTimer1="FT"+categoria+"1";

//----OFENDIDO---
ofendidoDial0="OD"+categoria+"0";
ofendidoDial1="OD"+categoria+"1";
ofendidoDial2="OD"+categoria+"2";
ofendidoDial3="OD"+categoria+"3";
//-----COMPLETE---
completeDial0="CD"+categoria+"0";
completeDial1="CD"+categoria+"1";
completeDial2="CD"+categoria+"2";
completeDial3="CD"+categoria+"3";
	
	  
//-----GOOD----
goodAnswerDial0="GAD"+categoria+"0";
goodAnswerDial1="GAD"+categoria+"1";
	
//---FAIL-----
failAnswerDial0="FAD"+categoria+"0";
failAnswerDial1="FAD"+categoria+"1";

//-----INTERACTION DIALOG----
interactionDial0="DI"+categoria+"0";
interactionDial1="DI"+categoria+"1";
interactionDial2="DI"+categoria+"2";
//------------------ANIMACION----------------
sabioSpriteTransformation=sprite_player_waiting_ans;
//-------------------------------------------
sabioSpriteIdleNormal=sprite_npc_kit_normal_idle;
sabioSpriteIdleStay=sprite_npc_kit_normal_idle;
//-------------------------------------------
sabioSpriteQuestNormal=sprite_test_sabio;
sabioSpriteQuestStay=sprite_npc_kit_normal_idle;
//-------------------------------------------
sabioSpriteIdleNormalTalk=sprite_npc_kit_normal_idle;
sabioSpriteIdleStayTalk=sprite_npc_kit_normal_idle;
//-------------------------------------------
sabioSpriteComplete=sprite_npc_kit_normal_idle;
sabioSpriteDead=sprite_npc_kit_normal_idle;
//---------------TEXT BOXT SABIO---------------
spriteTextBoxSabio=sprite_msm_sabioDemo ;
//---------------------------------------------
//--------------OBJETOS RESPUESTA--------------
//---------------------------------------------
objAnswer0=-1;
objAnswer1=-1;
objAnswer2=-1;
objAnswer3=-1;









//-------------------CONTROL NPC-SABIO-----------------
AccumFail=0;
AccumOfendido=0;
AccumTotalRight=0;  
intLimitQuestComplete=50;//LIMITE TO BOOL COMPLETE
numQuery=5;//N: (0:N-1)NUMERO DE QUEST.. EXISTEN X NIVEL-DENTRO DEL BANCO DE PREGUNTAS
intNivelQuest=1; //  nivel de las quest al inciar 



boolStay=false;
boolComplete=false;
boolOfendido=false;
//-----------------------------------------------------------------------------
//-------------------------------VALORES INICIALES-TO PLAY----------------------------
//-----------------------------------------------------------------------------
boolAnswer=0; // 0mal-1bien el jugador respondio correctamente o no, se reasigna mision

//---------ALMACENAR QUEST-TO NO REPEAT----------------
AccumRightArray[0]=""; // inicia con ninguna QUEST almacenada
AccumRightArrayTotal="";// SOLO PARA MOSTRAR DATOS
AccumRight=1 ; // CONTADOR NO ALMACENABLE inicia sin ninguna quest almacenada



                    
sabio_state="idle";

valueAnswer="";
valueScore="";
//_-------------------------TIMER----------------------------
boolTimer=false;
AccumTimer=60;//posible 60
Timelvl2=45;// posible 45
Timelvl3=30;//posible 30-final
valueTimer=AccumTimer;
boolDoorOutWithTimer=true;
///---------------------DIALOGO INTERACCION-------------------
boolTutorialDialog=true;
boolDialogInteraction=false;
boolFirstTimeDialog=true;
intCountDialogInteraction=1;
intCountDialogFirst=2;//DIALOGO DE INICIO AL VOLVER AL CARGAR EL JUEGO

boolFailTime=false;
intCountDialogTimer=0;



pagina = 0; // contador de pagina actual
//------PRIMER DIALOGO POR DEFECTO----------------------
paginas[0]= 0;

mostrarVentanaSabio=false; //mostrar cartel por defecto 
pagina= -1;    // pagina actial =-1, inactivo
///---------------------ASIGNACION DE MISION-QUERY------------
randomize();// REINICIA LA SEMILLA DEL RANDOM
query=round(random(numQuery)); //SE ASIGNA UNA QUEST RANDOM

//-----------------------------------------------------------------------------
//-------------------------------LOAD DATA----------------------------
//-script_load_sabio()----------------------------------------------------------------------------
script_load_sabio()
/*
ini_open("game_data.ini");

intNivelQuest=				ini_read_real("sabio-"+categoria,"nivelActual",intNivelQuest);
AccumTotalRight=			ini_read_real("sabio-"+categoria,"AccumRight",AccumTotalRight);

AccumFail=					ini_read_real("sabio-"+categoria,"AccumFail",AccumFail);
AccumOfendido=				ini_read_real("sabio-"+categoria,"AccumOfendido",AccumOfendido);

boolStay=					ini_read_string("sabio-"+categoria,"boolStay",boolStay);
boolComplete=				ini_read_string("sabio-"+categoria,"boolComplete",boolComplete);
boolOfendido=				ini_read_string("sabio-"+categoria,"boolOfendido",boolOfendido);

///---------------------DIALOGO INTERACCION-------------------
boolTutorialDialog=			ini_read_string("sabio-"+categoria,"boolTutorialDialog",boolTutorialDialog);
boolFirstTimeDialog=		ini_read_string("sabio-"+categoria,"boolFirstTimeDialog",boolFirstTimeDialog);
boolFailTime=				ini_read_string("sabio-"+categoria,"boolFailTime",boolFailTime);

intCountDialogInteraction=	ini_read_real("sabio-"+categoria,"intCountDialogInteraction",intCountDialogInteraction);
intCountDialogFirst=		ini_read_real("sabio-"+categoria,"intCountDialogFirst",intCountDialogFirst);
intCountDialogTimer=		ini_read_real("sabio-"+categoria,"intCountDialogTimer",intCountDialogTimer);
ini_close();*/
//


	