  event_inherited();			 
				 
				 /// @INTERACCION PLAYER
var keyAction=keyboard_check_pressed(global.controlAction);
	 //----------------------------------------------------------------------------
	 //----------------------INTERACCION 0 CON EL PLAYER--------------------------
	 //---------------------------------------------------------------------------
 //---------------------------------INTERACION CONTACTO---------------------------------
 if(keyAction  && distance_to_object(obj_player)<32){
	 mostrarVentanaSabio=true; //MUESTRA EL MODAL DE LA QUEST 
	 sabio_state="idle";//IDLE-(SUB IDLE-TALK)
	 
//---------	APAGA EL SOMBRERO-------(si esta on)----------------
if(obj_player.valueAnswer!="X"){
	obj_player.valueAnswer="X";
	}

//--------------------------------------------------------------
//--------------------------------------------------------------
//---------------------INTERACCION CON EL NPC-------------------
//--------------------------------------------------------------
 //----------CONTROL OFENDIDO SABIO------------------
 if(boolOfendido==true){
	 //instance_destroy();  TODOO
	 	paginas[0]=yd_lang(ofendidoDial0+"-"+string(AccumTotalRight));
	    paginas[1]=yd_lang(ofendidoDial1+"-"+string(AccumTotalRight));
		paginas[2]=yd_lang(ofendidoDial2+"-"+string(AccumTotalRight));
	    paginas[3]=yd_lang(ofendidoDial3+"-"+string(AccumTotalRight));
		//more if want---
	//--------------REINICIAR VALORES PARA RESPONDER---------------------------
	 obj_player.intActualQuest=-1;//DESHABILITA RESPUESTAS-----SIN QUEST ASIGNADA NO PUEDE RESPONDER
	 boolAnswer=0;// CORTA/REINICIA EL FLUJO DE RESPUESTA

	  
	 
	 
 }else{
   if(boolComplete==true){
	     //
		 //
		 //   DEFINIR PREMIO FINAL
		 //
		 // 
	   
	   //
	   //--------------INTERACCION KIT COMPLETE-------------------------
	   //
	   
		paginas[0]=yd_lang(completeDial0+"-"+string(AccumFail));
	    paginas[1]=yd_lang(completeDial1+"-"+string(AccumFail));
		paginas[2]=yd_lang(completeDial2+"-"+string(AccumFail));
	    paginas[3]=yd_lang(completeDial3+"-"+string(AccumFail));
		//todo more-----
		
	//--------------REINICIAR VALORES PARA RESPONDER---------------------------
	 obj_player.intActualQuest=-1;//DESHABILITA RESPUESTAS-----SIN QUEST ASIGNADA NO PUEDE RESPONDER
	 boolAnswer=0;// CORTA/REINICIA EL FLUJO DE RESPUESTA
	
	 

}else{
	   //
	   //--------------INTERACCION KIT FUNCIONAL-------------------------
	   //
	   
	   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////PAGINAS 0-1////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if(obj_player.intActualQuest==-1){
	
		if(!boolFailTime){
		 paginas[0]=yd_lang(dialogFirst0+"-"+string(intCountDialogFirst));
	     paginas[1]=yd_lang(dialogFirst1+"-"+string(intCountDialogFirst));
		}else{
		 paginas[0]=yd_lang(dialogFailTimer0+"-"+string(intCountDialogTimer));
	     paginas[1]=yd_lang(dialogFailTimer1+"-"+string(intCountDialogTimer));
		}
		 
		 
 script_restart_query();
}
   
    
   
//---------------------------------------------------------------------------------------------------------
//--------------------------------------RESPUESTA CORRECTA-------------------------------------------------
//---------------------------------------------------------------------------------------------------------
		 	 if(boolAnswer==1){
			//----------------------------------------------------
		     AccumTotalRight++;//CONTADOR --- ACUMULADO PREGUNTAS CORR.DEL SABIO
				 // SI VENIMOS CON LA RESPUESTA CORRECTA
				 paginas[0]=yd_lang(goodAnswerDial0+"-"+string(AccumTotalRight)+"-"+string(AccumFail));
				 paginas[1]=yd_lang(goodAnswerDial1+"-"+string(AccumTotalRight)+"-"+string(AccumFail));
			
			 //---------------------------------------------------
			 //Reinicia comprobador de respuesta correcta
			 AccumRightArray[AccumRight]=query;//SE ALMACENA LA QUEST RESPONDIDA BIEN
			 AccumRightArrayTotal+=string(AccumRightArray[AccumRight])+" ,";//SE PUEDE BORRAR-SOLO PARA COMPROBAR DATOS
			 AccumRight++;//INCREMENTA EL ARREGLO QUE ALMACENA QUEST HECHAS
			 
			 script_restart_query();
			 
		     
 			 //-----------------------------------------------------------------
			 
		 //
		 //
		 //   DEFINIR PREMIO X PREGUNTA
		 //
		 //
			 //score+=valueScore;CXD
			
			 
			 //--------------REINICIAR VALORES PARA RESPONDER---------------------------
			 boolAnswer=0;// CORTA/REINICIA EL FLUJO DE RESPUESTA
			 boolTimer=false;
			  
			 
			 
//---------------------------------------------------------------------------------------------------------
//-------------------------------------- RESPUESTA FALLIDA --------------------------------------------
//---------------------------------------------------------------------------------------------------------
	 }else if(boolAnswer==-1){
		 
		 
		 //
		 //
		 //   DEFINIR CASTIGO x PREGUNTA ERRONEA
		 // 
		 //
		 
		 
		 AccumFail++;
		 paginas[0]=yd_lang(failAnswerDial0+"-"+string(AccumFail)+"-"+string(AccumTotalRight));
	     paginas[1]=yd_lang(failAnswerDial1+"-"+string(AccumFail)+"-"+string(AccumTotalRight));
		 
		 script_restart_query();
			 
			 
			 //--------------REINICIAR VALORES PARA RESPONDER---------------------------
			boolAnswer=0;
			boolTimer=false;
	 }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////END PAGINAS 0-1 ////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------FIN DE CHECK CORRECT/FAIL RESPUESTA-----------------------------------
	 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////PAGINAS 2-3-4 ////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if(boolDialogInteraction){
//--------------------------------------------------------
//---------------DIALOGOS DE INTERACCION----------------------
//--------------------------------------------------------
		 obj_player.intActualQuest=-1;
		 
		 paginas[2]=yd_lang(interactionDial0+"-"+string(intCountDialogInteraction));
	     paginas[3]=yd_lang(interactionDial1+"-"+string(intCountDialogInteraction));
		 paginas[4]=yd_lang(interactionDial2+"-"+string(intCountDialogInteraction));
	     
	//	 paginas[2]="Lo has hecho super genial";
	//	 paginas[3]="si quieres tomar un descanso, \n ve a la villa por puticas";
	//	 paginas[4]="aca te estare esperando \n para ver si puedes con niveles mas pro";
		 
		 
  }else{
//--------------------------------------------------------
//---------------ASIGNACION DE QUEST----------------------
//--------------------------------------------------------
	//ASIGNAR QUEST EN EL DIALOGO DEL SABIO , paginas 2 y 3
	script_lib_sabio_quest();
	};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////END PAGINAS 2-3-4 ////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	};
	
  };
	 	 
 }
 
//---------------------------------------FIN INTERACION CONTACTO---------------------------------

    	
		
		
		
		
		
		
 //------------------------------------------------------------------------------------------------------------
 //-----------------------------------------------TIMER------------------------------------------------------
 //--------------------------------------------------------------------------------------------------------

 
 
 
if(boolTimer==true){ //Cuando activan el Timer se activa el contador
	
	if(instance_exists(obj_door_padre) ){
	if(obj_door_padre.doorState=="open" && boolDoorOutWithTimer){
		
		 AccumFail++;
		 boolFailTime=true;
	     intCountDialogTimer=1;//Hacer dialogo, Ah, es que has intentado huir
		 script_restart_query();
		
	     script_save_sabio();
		boolDoorOutWithTimer=false;
	}
	}
	
	if(valueTimer>0){
	 
		 valueTimer=valueTimer-(delta_time/1000000); //Descuenta el tiempo, contador a 0 
		 
	}else{

	 

		 //
		 //   DEFINIR CASTIGO x PREGUNTA ERRONEA
		 //
		 //
		 mostrarVentanaSabio=false;
         AccumFail++;
		 boolFailTime=true;
	     intCountDialogTimer=1;
		 script_restart_query();
		
	     script_save_sabio();
			  //--------------REINICIAR VALORES PARA RESPONDER---------------------------
			boolAnswer=0;
			boolTimer=false;//apaga el reloj
			sabio_state="idle"; //pone al npc en su estado normal/no query
			obj_player.valueAnswer="X"; //apaga el sombrero
			obj_player.intActualQuest=-1; //INAHILITA RESPUESTAS-PERDIO QUERY
	}}
//----------REINICIA TIMER--------------------
 if(boolTimer==false){
 valueTimer=AccumTimer;

 }
 



//-------------------------------------------------------------------------------------------------
//----------------------------------CONTROL LIFE --- KIT--------------------------------------
//-------------------------------------------------------------------------------------------------



//-------------------------------------------
//------CONTROL NIVEL DE QUEST--- KIT--------
//-------------------------------------------
switch(AccumTotalRight){
case 0:
	//NIVEL 1
break;
case 5:
	//NIVEL 2	
		intNivelQuest=2;
break; 
case 10:
	//NIVEL 3
	intNivelQuest=3;
	AccumTimer=Timelvl2;
break;	
case 15:
	//NIVEL 4
	intNivelQuest=4;
break;
case 20:
	//NIVEL 5
	intNivelQuest=5;
break;
case 25:
	//NIVEL 6
	intNivelQuest=6;
	boolStay=true;
	AccumTimer=Timelvl3;
break;
case 30:
	//NIVEL 7
	intNivelQuest=7;
break;
case 35:
	//NIVEL 8
	intNivelQuest=8;
break;
case 40:
	//NIVEL 9
	intNivelQuest=9;
break;
case 45:
	//NIVEL 10
	intNivelQuest=10;
break;
case intLimitQuestComplete:
	boolComplete=true;
break;
}












//-------------------------------------------
//------CONTROL MUERTE SABIO--- KIT--------
//-------------------------------------------
  if(AccumFail>=3){
	  //IF STAY FALSE
	  if(boolStay==false){
		   //REINICIA VALORES DE JUEGO- CUENTA OFENSAS 
						    AccumOfendido++;
							AccumFail=0;
							AccumRight=1;
							AccumTotalRight=0;
							intNivelQuest=1;
							for(i=0; i < array_length_1d(AccumRightArray);i++){
								AccumRightArray[i]="";
							}
							AccumRightArrayTotal="";
	  }else{
		  
		  
		 //
		 //   DEFINIR CASTIGO x ACUMULACION DE ERRORES en stado Stay
		 //
		 //
		 
	   //REINICIA VALORES DE JUEGO- NO AUMENTA OFENSAS
						    
							AccumFail=0;
							AccumRight=25;
							AccumTotalRight=25;
							intNivelQuest=5;
							for(i=25; i < array_length_1d(AccumRightArray);i++){
								AccumRightArray[i]="";
							}
							AccumRightArrayTotal="";
	  }
	  
	  
	
			};
//---OFENDIDO XMAX FAIL		
if(AccumOfendido>=3){
	//--- se ofende para cuando vuelva al contacto	
boolOfendido=true;	
}








//-------------------------------------------
//------CONTROL DIALOGOS INTERACCION--- KIT--------
//-------------------------------------------
/*
switch(AccumTotalRight){
case 0:
		if(boolTutorialDialog){
//TODO TUTORIAL DIALOG O DIALOGO EXPLICATIVO DEL MODO DE JUEGO 
		intCountDialogInteraction=3;
		intCountDialogFirst=1;
			
		boolDialogInteraction=true;											//ASIGNAR -CREAR DIALOGOS ADECUADOS, pues los actuales son de prueba durisimo
		
		}
		//TODO PRIMER DIALOGO LUEGO DEL TUTORIAL
		if(boolFirstTimeDialog && !boolTutorialDialog){
		intCountDialogFirst=0;  //PRIMER DIALOGO LUEGO DE TUTORIAL
		boolFirstTimeDialog=false;
		}
		
		
break;
	

case 5:
	//NIVEL 2
	
		if(boolFirstTimeDialog){// ---start----INTERACCION
		intCountDialogInteraction=1;//RESPUESTA 2-3-4 DESPUES DE GOOD AMSWER
		intCountDialogFirst=1;//FIRST DIALOG 0-1, luego de volver de FREETIME
		
		boolDialogInteraction=true; // ACTIVA PARA REEMPLAZAR LA INTERACCION, 
		boolFirstTimeDialog=false;//----end------
		}
break;

case 10:
	//NIVEL 3
	if(AccumFail==2){//ejemplo de condicion adicional particular para dialogo
	if(boolFirstTimeDialog){
		intCountDialogInteraction=1;
		intCountDialogFirst=1;
		
		boolDialogInteraction=true;
		boolFirstTimeDialog=false;
		}}
break;	


case 15:
	//NIVEL 4
	if(boolFirstTimeDialog){
		intCountDialogInteraction=2;
		intCountDialogFirst=2;
		
		boolDialogInteraction=true;
		boolFirstTimeDialog=false;	
		}
	break;
case 20:
	//NIVEL 5
break;
case 25:
	//NIVEL 6	
break;

default:
intCountDialogFirst=0;// HACER DIALOGO POR DEFAULT PARA CUANDO ME SEPARE REINICIANDO EL JUEGO O ALGUNA OTRA INTERRUPCION INESPERADA DE LAS PREGUNTAS
if(!boolFirstTimeDialog){boolFirstTimeDialog=true;}
break;
}
*/

























//----------------------------------------------------------------------------------------------------------------
//------------------------------------------------CUADRO DE DIALOGO -----------------------------------------------
//----------------------------------------------------------------------------------------------------------------
//------  CIERRA EL MODAL--X DISTANCIA----------------------------------------------------------------------------------------------------------------------------------------
 if (distance_to_object(obj_player)>32  && mostrarVentanaSabio ){
		mostrarVentanaSabio=false;
		pagina=-1; 
		
		
		
		
		if(pagina<2){
		obj_player.intActualQuest=-1
		if(!boolDoorOutWithTimer){boolDoorOutWithTimer=true}
		}else{
		 if(boolFailTime){boolFailTime=false;}
			if(obj_player.intActualQuest!=-1){//------HABILITA RESPUESTAS-----Comprueba que el player tiene una query asignada
				//----------------STAR QUEST------------------------
			 if(obj_player.intActualQuest==query)//COMPRUEBA SI LA query asignada es la ACTUAL
			 {
			 sabio_state="quest";
			 script_asignar_answer(objAnswer0,objAnswer1,objAnswer2,objAnswer3); 
			 boolTimer=true;
				 };
			 
			 };
		}
		
	 if(boolDialogInteraction){
	 boolDialogInteraction=false;
	 paginas=0;
	 }
	//ESTADO DEATH 
	if(boolOfendido==true){sabio_state="dead";}
	//ESTADO WON
	if(boolComplete==true){sabio_state="complete";}
	script_save_sabio();
		//SI ESTA EN LAS PAGINAS DE LAS QUEST TODO
		
 };
 //--------CIERRA EL MODAL----X LIMITE DE PAGINAS-----------------------------------------------------------------------------------------------------------------------------
 if(keyAction && mostrarVentanaSabio ){
    if(pagina+1<array_length_1d(paginas)){
	 pagina++;
	 }else{
	 //Si se pasa el maximo de paginas reincia el cartel 
	 mostrarVentanaSabio=false;
	 pagina=-1; 
	 
	 if(boolFailTime){boolFailTime=false;}//
	 if(boolTutorialDialog){boolTutorialDialog=false;}
	 if(!boolDoorOutWithTimer){boolDoorOutWithTimer=true}
	 
	 
	 if(boolDialogInteraction){
	 boolDialogInteraction=false;
	 paginas=0;
	 }
	 //-----------------------------------------------
	 //----ACTIVAR AL CERRAR LA VENTANA DE DIALOGO----
	 //-----------------------------------------------
	 
	 //guardar categoria,intNivelQuest,AccumTotalRight,AccumFail,AccumOfendido,boolStay,boolComplete,boolOfendido,boolTutorialDialog,boolFirstTimeDialog,intCountDialogInteraction,intCountDialogFirst,boolFailTime,intCountDialogTimer
	script_save_sabio();

	 //----------------STAR QUEST------------------------
		if(obj_player.intActualQuest!=-1){//------HABILITA RESPUESTAS-----Comprueba que el player tiene una query asignada
			 script_asignar_answer(objAnswer0,objAnswer1,objAnswer2,objAnswer3); 	
			 boolTimer=true;
		 };
	//---------------------------------------------------
	
	//-----Cambia los Estados al Cerrar ventana de dialogo
	//ESTADO QUEST
	if(obj_player.intActualQuest==query){sabio_state="quest";};
	//ESTADO DEATH 
	if(boolOfendido==true){sabio_state="dead";}
	//ESTADO WON
	if(boolComplete==true){sabio_state="complete";}
     }
 }   
  
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
//--------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------
//------------------------------------------------ANIMACION -----------------------------------------------
//-----------------------------------------------------------------------------------------------------------

//------------STATE MACHINE---------------

switch(sabio_state){
	
 case "idle": 
		if(boolStay==false){
			// ------ NORMAL -------
			if(mostrarVentanaSabio){
				
				sprite_index=sabioSpriteIdleNormalTalk;
				
			}else{
				
				if(sprite_index!=sabioSpriteIdleNormal )	{
						if(image_index>= image_number-1){
					sprite_index=sabioSpriteTransformation;
						}
					if(image_index>= image_number-1){
					sprite_index=sabioSpriteIdleNormal;
					}
				}
			}             
			
		}else{
			//---------STAY---------
			
		if(mostrarVentanaSabio){
			
			sprite_index=sabioSpriteIdleStayTalk;
			
			}else{
			if(sprite_index!=sabioSpriteIdleStay){
					if(image_index>= image_number-1){
				sprite_index=sabioSpriteTransformation;
					}
				if(image_index>= image_number-1){
					sprite_index=sabioSpriteIdleStay;
				}
			}
		}
		}	
		image_speed= 0.5;
 break;


 case "quest":
		if(boolStay==false){
			
			// ------ NORMAL -------
			if(sprite_index!=sabioSpriteQuestNormal){
				/*if(image_index>= image_number-1){
					sprite_index=sabioSpriteTransformation;
				}*/
	if(image_index>= image_number-1){
	sprite_index=sabioSpriteQuestNormal;
	}}
			
		}else{
			//---------STAY---------
			if(sprite_index!=sabioSpriteQuestStay){
					/*if(image_index>= image_number-1){
						sprite_index=sabioSpriteTransformation;
					}*/
				if(image_index>= image_number-1){
				sprite_index=sabioSpriteQuestStay;
					}}
	
					}
		image_speed= 0.5;
 break;


 case"dead":
 
		if(sprite_index!=sabioSpriteDead){
	sprite_index=sabioSpriteTransformation;
	if(image_index>= image_number-1){
	sprite_index=sabioSpriteDead;
	}}
		
		image_speed= 0.5;
 break;


 case"complete":
		if(sprite_index!=sabioSpriteComplete){
	sprite_index=sabioSpriteTransformation;
	if(image_index>= image_number-1){
	sprite_index=sabioSpriteComplete;
	}}
		image_speed= 0.5;
 break;
	
	

}


