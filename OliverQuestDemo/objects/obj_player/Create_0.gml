
//------------------------------------------------
//--------------ESTADOS POR DEFECTO---------------
str_state="idle";
image_speed= 0.1;
depth=-1;


boolPause=false;
boolInDialog=false;
boolControlActive=true;
//-----------VARIABLES DE MOVIMIENTO--------
intVY=0;// VELOCIDAD VERTICAL
intVX=0;// VELOCIDAD HORIZONTAL
intMove=0;//INDICA LA DIRECCION A DONDE NOS MOVEMOS

intM= 1.0 ;//Multiplicador //*global.bolPause

intVYMax = 9 * intM; // GRAVEDAD  
intVXMax= 2.5 * intM; //LIMITE VELOCIDAD

intJumpHeight = 8 * intM; //SALTO
intGravityNorm= 0.5* intM; //INCREMENTO DE LA GRAVEDAD-FRICCION 
intGravitySlide= 0.15*intM; // GRAVEDAD MIENTRAS ESTAMOS EN UNA PARED
intGroundAcc= 1.0 *intM; // ACELERACION CORRIENDO 
intGroundFric= 1.9 *intM; //FRRICION DEL SUELO CORRIENDO 
intAirAcc = 0.75 * intM; // ACELERACION EN EL AIRE
intAirFric = 0.1 *intM; // FRICCION DEL AIRE

//---------------------SALTO-------------
intJumpDoble=1; // LIMITE DE SALTOS 
//---------------------ATAQUE------------
bolKeyActive=true; // INDICA QUE TENEMOS PERMITICO DAR UN GOLPE
intPunchDir=1; // INDICA LA DIRECCION A DONDE ES DIRIGIDO EL GOLPE 

//---------------- GLOBAL GAME INTERACTION------------------
//---COIN- MONEDAS ACTUALES
score=0;

//QUEST-MISIONES-PREGUNTAS QUE ESTA RESPONDIENDO -TOODOO
valueAnswer="X";// ALMACENA LA RESPUESTA ACTIVA- NO LA CORRECTA. LA RESPONDIDA ACTUAL


intActualQuest=-1;//NO SE USA -(SELLAMA EN KIT)  numero de la mision actual-

//intRightVideoGames=0;// contador x genero 
//intRightAnime=0;
//intRightMovies=0;
//intRightGeography=0;
//TODO

// TODO 




//----------------------CREATION FOR DOORS-------------------------------
//en el menu principal y demas siempre sera -1 , solo tendra valor recien usada una puerta 
if(global.lastDoor!=-1){
 script_player_lastDoor();
 }

//----------------------------------------SAVE-LOAD
//----------------------------------------
if(global.boolLoadGame){
if(room_get_name(room)!="room_main_menu"  
&&  room_get_name(room)!="room_menu_settings"){				
	x=global.coordXPlayer;
	y=global.coordYPlayer-32;
	
				}	
	global.boolLoadGame=false;
}
	
 
//------------CREATE LIEBRE TO FOLLOW TO CAM------
instance_create_depth(x,y,0,obj_liebre);
 shadow = instance_create_depth(x,y,depth-1,obj_shadow_follow);
 shadow.follow= object_index;
// cREATE TIMER GLOBAL OBJET
if(room_get_name(room)!="room_main_menu"  
&&  room_get_name(room)!="room_menu_settings"){
	instance_create_depth(0,0,0,obj_time);
}
	
	