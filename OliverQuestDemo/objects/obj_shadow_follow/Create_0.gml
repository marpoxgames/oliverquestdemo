/// @description Insert description here
// You can write your code in this editor



follow = -1;
depth = follow.depth + 1;
anchoSh=10;
altoSh=5;
porcSh = -1;

//-----------VARIABLES DE MOVIMIENTO--------
intVY=0;// VELOCIDAD VERTICAL
intVX=0;// VELOCIDAD HORIZONTAL
intMove=0;//INDICA LA DIRECCION A DONDE NOS MOVEMOS

intM= 1.0 ;//Multiplicador //*global.bolPause

intVYMax = 9 * intM; // GRAVEDAD  
intVXMax= 2.5 * intM; //LIMITE VELOCIDAD

intJumpHeight = 8 * intM; //SALTO
intGravityNorm= 0.5* intM; //INCREMENTO DE LA GRAVEDAD-FRICCION 
intGravitySlide= 0.15*intM; // GRAVEDAD MIENTRAS ESTAMOS EN UNA PARED
intGroundAcc= 1.0 *intM; // ACELERACION CORRIENDO 
intGroundFric= 1.9 *intM; //FRRICION DEL SUELO CORRIENDO 
intAirAcc = 0.75 * intM; // ACELERACION EN EL AIRE
intAirFric = 0.1 *intM; // FRICCION DEL AIRE
