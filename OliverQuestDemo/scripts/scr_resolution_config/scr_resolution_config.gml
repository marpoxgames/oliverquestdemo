function scr_resolution_config() {
	// SET THE RESOLUTION WE WANT TO DISPLAY, OR GET THIS VALUE FROM THE DEVICE
	var displayWidth = global.displayWidth; // display_get_width();
	var displayHeight = global.displayHeight; // display_get_height();
	var fullScreen = global.displayFull;



	//---------------PANTALLA COMPLETA OFF----------------------------
	if(!fullScreen){
	window_set_fullscreen(false);

	// SET THE SIZE OF THE GAME WINDOW 
	window_set_size(displayWidth,displayHeight);

	// SET THE RESOLUTION WE`VE PROGRAMMED THE GAME FOR 

	// DETERMINE THE ASPECT RATIO -SEGUN VENTANA
	var bWidth = displayWidth;
	var bHeight = displayHeight;
	var baseWidth =0;
	var baseHeight = 0;
	var ratio = bWidth/bHeight;
	switch(ratio){
		case 16/9:
		baseWidth =320;
	    baseHeight = 200;
		break;
		case 4/3:
		baseWidth =320;
		baseHeight = 240;
		break;
		default:
		baseWidth =320;
		baseHeight = 200; 
		break;
	}
	// SET THE SIZE OF -GUI-
	display_set_gui_size(baseWidth*2,baseHeight*2);

	// DETERMINE THE ASPECT RATIO 
	var aspect = baseWidth/baseHeight;


	// WORK OUT THE ADJUSTED HEIGHT AND WIDTH
	if(displayWidth >= displayHeight)
	{
	 var height = min(baseHeight, displayHeight);
	 var width	= height * aspect;
	}

	// RESIZE THE APPLICATION SURFACE OF OUR ADJUSTES VALUES
	surface_resize(application_surface, width, height);








	}else{
	//---------------PANTALLA COMPLETA ON----------------------------
	window_set_fullscreen(true);
	//TOMAMOS LA MEDIDA DE LA VENTANA PARA CALCULAR EL RATIO

	var bWidth = display_get_width();
	var bHeight = display_get_height();

	// DETERMINE THE ASPECT RATIO -SEGUN PANTALLA
	var baseWidth =0;
	var baseHeight = 0;
	var ratio = bWidth/bHeight;
	switch(ratio){
		case 16/9:
		baseWidth =320;
	    baseHeight = 200;
		break;
		case 4/3:
		baseWidth =320;
		baseHeight = 240;
		break;
		default:
		baseWidth =320;
		baseHeight = 200; 
		break;
		}
	
	// SET THE SIZE OF -GUI-
	display_set_gui_size(baseWidth*2,baseHeight*2);


	// SET THE RESOLUTION WE`VE PROGRAMMED THE GAME FOR 


	surface_resize(application_surface, baseWidth, baseHeight);

	
	//------------OPTIONAL-------------------------------
	//surface_resize(application_surface, bWidth, bHeight);
	
	
	}





}
