function script_check_colision(argument0, argument1) {

	//---------------------------------------------------------------------------------------
	//----------------------------------COLISIONES-----------------------------------------
	//---------------------------------------------------------------------------------------

	var intVX=argument0;

	var intVY=argument1;



	//Colision Horizontal 
	repeat(abs(intVX)){
		//Cuesta Arriba
		if(place_meeting(x + sign(intVX),y,obj_block) && place_meeting(x + sign(intVX), y-1,obj_block) && place_meeting(x + sign(intVX), y-2,obj_block) && !place_meeting(x + sign(intVX), y-3,obj_block)){
			y-=3;
		}else if(place_meeting(x + sign(intVX),y,obj_block) && place_meeting(x + sign(intVX), y-1,obj_block) && !place_meeting(x + sign(intVX), y-2,obj_block)){
			y-=2;
		}else if(place_meeting(x + sign(intVX),y,obj_block) && !place_meeting(x + sign(intVX), y-1,obj_block)){
			y--;
		}
		//Cuesta Abajo
		if(!place_meeting(x + sign(intVX),y,obj_block) && !place_meeting(x + sign(intVX), y+1,obj_block) && !place_meeting(x+sign(intVX),y+2,obj_block) && !place_meeting(x+sign(intVX),y+3,obj_block) && place_meeting(x+sign(intVX),y+4,obj_block)){
			y+=3;
		}else if(!place_meeting(x + sign(intVX),y,obj_block) && !place_meeting(x + sign(intVX), y+1,obj_block) && !place_meeting(x+sign(intVX),y+2,obj_block) && place_meeting(x+sign(intVX),y+3,obj_block)){
			y+=2;
		}else if(!place_meeting(x + sign(intVX),y,obj_block) && !place_meeting(x + sign(intVX), y+1,obj_block) && place_meeting(x+sign(intVX),y+2,obj_block)){
			y++;
		}
		//Colision Normal bloque
		if(!place_meeting(x + sign(intVX) ,y, obj_block))
		{
		x += sign(intVX);
		}else{
		intVX =0;
		break;
	}};


	//Colision Vertical 
	repeat(abs(intVY)){
	if(place_meeting(x ,y+sign(intVY), obj_block))
	{
		 intVY =0;
		 break;
	}
	else if//Colision con plataforma UniDireccional 
	(place_meeting(x,y +sign(intVY),obj_plataform) && !place_meeting(x,y,obj_plataform) && (intVY>=0)){
	 intVY=0;
	 break;
	 //Colision con plataforma MOVIL UniDireccional
	}else  if((place_meeting(x,(y+sign(intVY)+1),obj_plataform_movilVer) && !place_meeting(x,y,obj_plataform_movilVer)) && (intVY>=0)){
	 intVY = instance_place(x,(y+sign(intVY)+1),obj_plataform_movilVer).intVY;

	      break;
	}else{ 
	     y += sign(intVY); 
	}} 


}
