function script_load_sabio() {

	ini_open("game_data.ini");

	intNivelQuest=				ini_read_real("sabio-"+categoria,"nivelActual",intNivelQuest);
	AccumTotalRight=			ini_read_real("sabio-"+categoria,"AccumRight",AccumTotalRight);

	AccumFail=					ini_read_real("sabio-"+categoria,"AccumFail",AccumFail);
	AccumOfendido=				ini_read_real("sabio-"+categoria,"AccumOfendido",AccumOfendido);

	boolStay=					ini_read_string("sabio-"+categoria,"boolStay",boolStay);
	boolComplete=				ini_read_string("sabio-"+categoria,"boolComplete",boolComplete);
	boolOfendido=				ini_read_string("sabio-"+categoria,"boolOfendido",boolOfendido);

	///---------------------DIALOGO INTERACCION-------------------
	boolTutorialDialog=			ini_read_string("sabio-"+categoria,"boolTutorialDialog",boolTutorialDialog);
	boolFirstTimeDialog=		ini_read_string("sabio-"+categoria,"boolFirstTimeDialog",boolFirstTimeDialog);
	boolFailTime=				ini_read_string("sabio-"+categoria,"boolFailTime",boolFailTime);

	intCountDialogInteraction=	ini_read_real("sabio-"+categoria,"intCountDialogInteraction",intCountDialogInteraction);
	intCountDialogFirst=		ini_read_real("sabio-"+categoria,"intCountDialogFirst",intCountDialogFirst);
	intCountDialogTimer=		ini_read_real("sabio-"+categoria,"intCountDialogTimer",intCountDialogTimer);
	ini_close();


}
