function script_player_lastDoor() {
	var lastDoor= global.lastDoor;
	var otherDoor=-1;

	switch(lastDoor)
	{
		//SABIO DEMO
		case "obj_door_kit_in":
		otherDoor=obj_door_kit_out;
		if(instance_exists(otherDoor)){
			x=otherDoor.x;
			y=otherDoor.y-32;
		}
		break;
	
		case "obj_door_kit_out":
		otherDoor=obj_door_kit_in;
		if(instance_exists(otherDoor)){
			x=otherDoor.x;
			y=otherDoor.y-32;
		}
		break;
		//OLIVER TEMPLE DEMO
		case "obj_door_oliverTemple_in":
		otherDoor=obj_door_oliverTemple_out;
		if(instance_exists(otherDoor)){
			x=otherDoor.x;
			y=otherDoor.y-32;
		}
		break;
	
		case "obj_door_oliverTemple_out":
		otherDoor=obj_door_oliverTemple_in;
		if(instance_exists(otherDoor)){
			x=otherDoor.x;
			y=otherDoor.y-32;
		}
		break;
		//TODO MORE DOORS
	}


	script_player_save_cord(x,y);
	global.lastDoor=-1;
	ini_open("game_data.ini");
		ini_write_string("player-data","lastDoor",-1);
	ini_close();


}
