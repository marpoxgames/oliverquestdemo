function script_save_sabio() {

	/*var categoria = argument0; 

	var intNivelQuest = argument1;
	var AccumTotalRight=argument2;

	var AccumFail= argument3;
	var AccumOfendido= argument4;

	var boolStay=argument5;
	var boolComplete=argument6;
	var boolOfendido=argument7;

	var boolTutorialDialog=argument8
	var boolFirstTimeDialog=argument9
	var intCountDialogInteraction=argument10;
	var intCountDialogFirst=argument11;
	var boolFailTime=argument12;
	var intCountDialogTimer=argument13;
	*/



	ini_open("game_data.ini");

	ini_write_real("sabio-"+categoria,"nivelActual",intNivelQuest);
	ini_write_real("sabio-"+categoria,"AccumRight",AccumTotalRight);

	ini_write_real("sabio-"+categoria,"AccumFail",AccumFail);
	ini_write_real("sabio-"+categoria,"AccumOfendido",AccumOfendido);

	ini_write_string("sabio-"+categoria,"boolStay",boolStay);
	ini_write_string("sabio-"+categoria,"boolComplete",boolComplete);
	ini_write_string("sabio-"+categoria,"boolOfendido",boolOfendido);

	ini_write_string("sabio-"+categoria,"boolTutorialDialog",boolTutorialDialog);
	ini_write_string("sabio-"+categoria,"boolFirstTimeDialog",boolFirstTimeDialog);
	ini_write_string("sabio-"+categoria,"boolFailTime",boolFailTime);

	ini_write_real("sabio-"+categoria,"intCountDialogInteraction",intCountDialogInteraction);
	ini_write_real("sabio-"+categoria,"intCountDialogFirst",intCountDialogFirst);
	ini_write_real("sabio-"+categoria,"intCountDialogTimer",intCountDialogTimer);


	ini_close();


}
