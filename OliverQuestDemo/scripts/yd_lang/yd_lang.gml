function yd_lang(argument0) {
	// return locale sting for a given key 

	var key = argument0;
	if(ds_map_exists(global.yd_locale_words,key))
	{
		return ds_map_find_value(global.yd_locale_words,key);
	
	}else{
		return "??MISSING TRANSLATION??";	
	}


}
