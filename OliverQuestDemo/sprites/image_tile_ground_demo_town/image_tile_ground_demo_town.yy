{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 447,
  "bbox_top": 107,
  "bbox_bottom": 255,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 448,
  "height": 256,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d8fd6789-4542-4a8f-8c2b-5f5f22e496a4","path":"sprites/image_tile_ground_demo_town/image_tile_ground_demo_town.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d8fd6789-4542-4a8f-8c2b-5f5f22e496a4","path":"sprites/image_tile_ground_demo_town/image_tile_ground_demo_town.yy",},"LayerId":{"name":"261b3ef8-cfe6-453f-9739-616493342262","path":"sprites/image_tile_ground_demo_town/image_tile_ground_demo_town.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"image_tile_ground_demo_town","path":"sprites/image_tile_ground_demo_town/image_tile_ground_demo_town.yy",},"resourceVersion":"1.0","name":"d8fd6789-4542-4a8f-8c2b-5f5f22e496a4","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"image_tile_ground_demo_town","path":"sprites/image_tile_ground_demo_town/image_tile_ground_demo_town.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"24b109a8-f9de-4471-a735-d3b9eb38ebed","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d8fd6789-4542-4a8f-8c2b-5f5f22e496a4","path":"sprites/image_tile_ground_demo_town/image_tile_ground_demo_town.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"image_tile_ground_demo_town","path":"sprites/image_tile_ground_demo_town/image_tile_ground_demo_town.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"261b3ef8-cfe6-453f-9739-616493342262","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GROUNDS",
    "path": "folders/ASSETs/Sprites/TILES/GROUNDS.yy",
  },
  "resourceVersion": "1.0",
  "name": "image_tile_ground_demo_town",
  "tags": [],
  "resourceType": "GMSprite",
}