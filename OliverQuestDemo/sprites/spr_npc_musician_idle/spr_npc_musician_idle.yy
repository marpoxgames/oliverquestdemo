{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 7,
  "bbox_right": 54,
  "bbox_top": 4,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"31e9104c-dc97-4384-8055-12cd8ec96e9f","path":"sprites/spr_npc_musician_idle/spr_npc_musician_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"31e9104c-dc97-4384-8055-12cd8ec96e9f","path":"sprites/spr_npc_musician_idle/spr_npc_musician_idle.yy",},"LayerId":{"name":"ffb2932c-37cc-417a-8b25-405bf75eaf3f","path":"sprites/spr_npc_musician_idle/spr_npc_musician_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_npc_musician_idle","path":"sprites/spr_npc_musician_idle/spr_npc_musician_idle.yy",},"resourceVersion":"1.0","name":"31e9104c-dc97-4384-8055-12cd8ec96e9f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_npc_musician_idle","path":"sprites/spr_npc_musician_idle/spr_npc_musician_idle.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"008c2ae7-2ec3-498b-b7d8-3a4d5886a076","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"31e9104c-dc97-4384-8055-12cd8ec96e9f","path":"sprites/spr_npc_musician_idle/spr_npc_musician_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 30,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_npc_musician_idle","path":"sprites/spr_npc_musician_idle/spr_npc_musician_idle.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"ffb2932c-37cc-417a-8b25-405bf75eaf3f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "MUSICIAN",
    "path": "folders/Sprites/SPRITES/NPC`s/DEMO TOWN/MUSICIAN.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_npc_musician_idle",
  "tags": [],
  "resourceType": "GMSprite",
}