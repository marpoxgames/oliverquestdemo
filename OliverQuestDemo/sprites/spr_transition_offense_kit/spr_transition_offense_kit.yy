{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 21,
  "bbox_right": 100,
  "bbox_top": 7,
  "bbox_bottom": 61,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 128,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d95f162f-e8c7-4773-85ab-1458c78329a3","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d95f162f-e8c7-4773-85ab-1458c78329a3","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":{"name":"e359d760-6bd9-4985-bfa9-0a222a85ed5f","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_transition_offense_kit","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"d95f162f-e8c7-4773-85ab-1458c78329a3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0b4cb576-1fc6-497a-b614-b9255845a940","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0b4cb576-1fc6-497a-b614-b9255845a940","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":{"name":"e359d760-6bd9-4985-bfa9-0a222a85ed5f","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_transition_offense_kit","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"0b4cb576-1fc6-497a-b614-b9255845a940","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ae1839c5-8e4b-4c5a-bd20-4267f87b84c7","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ae1839c5-8e4b-4c5a-bd20-4267f87b84c7","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":{"name":"e359d760-6bd9-4985-bfa9-0a222a85ed5f","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_transition_offense_kit","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"ae1839c5-8e4b-4c5a-bd20-4267f87b84c7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b2dd8934-0640-42a6-836d-b20e4c5517ae","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b2dd8934-0640-42a6-836d-b20e4c5517ae","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":{"name":"e359d760-6bd9-4985-bfa9-0a222a85ed5f","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_transition_offense_kit","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"b2dd8934-0640-42a6-836d-b20e4c5517ae","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"25a98273-3675-48e8-b2df-1a323197648f","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"25a98273-3675-48e8-b2df-1a323197648f","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"LayerId":{"name":"e359d760-6bd9-4985-bfa9-0a222a85ed5f","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_transition_offense_kit","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","name":"25a98273-3675-48e8-b2df-1a323197648f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_transition_offense_kit","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3690f634-435b-44cf-882c-dbafc0dfea16","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d95f162f-e8c7-4773-85ab-1458c78329a3","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dee6b067-9078-4425-ba01-8b66d1beaf60","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0b4cb576-1fc6-497a-b614-b9255845a940","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f8d7ce53-dd01-4b17-9598-9ed18a9cc625","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ae1839c5-8e4b-4c5a-bd20-4267f87b84c7","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2bfe9b64-09c4-4a63-a6bb-3e86367e2d87","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b2dd8934-0640-42a6-836d-b20e4c5517ae","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"17362ad2-6b3c-4d7d-9ef9-89e735d642f5","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"25a98273-3675-48e8-b2df-1a323197648f","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 63,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_transition_offense_kit","path":"sprites/spr_transition_offense_kit/spr_transition_offense_kit.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e359d760-6bd9-4985-bfa9-0a222a85ed5f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "COUNT",
    "path": "folders/Sprites/SPRITES/SABIOS/KIT/OFFENSE´s/COUNT.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_transition_offense_kit",
  "tags": [],
  "resourceType": "GMSprite",
}