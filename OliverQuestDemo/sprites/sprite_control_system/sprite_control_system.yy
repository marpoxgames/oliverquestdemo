{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 36,
  "bbox_top": 4,
  "bbox_bottom": 56,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"97156183-677a-4682-88fe-408a5b3800a7","path":"sprites/sprite_control_system/sprite_control_system.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"97156183-677a-4682-88fe-408a5b3800a7","path":"sprites/sprite_control_system/sprite_control_system.yy",},"LayerId":{"name":"4ed4297d-0b07-45fe-b555-4d37dcb5da35","path":"sprites/sprite_control_system/sprite_control_system.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_control_system","path":"sprites/sprite_control_system/sprite_control_system.yy",},"resourceVersion":"1.0","name":"97156183-677a-4682-88fe-408a5b3800a7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite_control_system","path":"sprites/sprite_control_system/sprite_control_system.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"577607b3-7d04-40d2-8535-fe5cd652ff95","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"97156183-677a-4682-88fe-408a5b3800a7","path":"sprites/sprite_control_system/sprite_control_system.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite_control_system","path":"sprites/sprite_control_system/sprite_control_system.yy",},
    "resourceVersion": "1.3",
    "name": "sprite_control_system",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4ed4297d-0b07-45fe-b555-4d37dcb5da35","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GLOBAL",
    "path": "folders/Sprites/SPRITES/GLOBAL.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite_control_system",
  "tags": [],
  "resourceType": "GMSprite",
}