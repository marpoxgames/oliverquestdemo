{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 4,
  "bbox_top": 3,
  "bbox_bottom": 4,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 8,
  "height": 8,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f2eaf341-b4c1-4094-9006-c1c16c7dd76f","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f2eaf341-b4c1-4094-9006-c1c16c7dd76f","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":{"name":"dfa6078b-0891-450a-b4d0-440a510fead7","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"f2eaf341-b4c1-4094-9006-c1c16c7dd76f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3224d7c9-2900-40ee-bd9b-3c217583f806","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3224d7c9-2900-40ee-bd9b-3c217583f806","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":{"name":"dfa6078b-0891-450a-b4d0-440a510fead7","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"3224d7c9-2900-40ee-bd9b-3c217583f806","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"782e59b4-3a43-48ca-aa87-c9913b68542c","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"782e59b4-3a43-48ca-aa87-c9913b68542c","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":{"name":"dfa6078b-0891-450a-b4d0-440a510fead7","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"782e59b4-3a43-48ca-aa87-c9913b68542c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"215824eb-db0e-4646-aa2a-c38337e820b5","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"215824eb-db0e-4646-aa2a-c38337e820b5","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":{"name":"dfa6078b-0891-450a-b4d0-440a510fead7","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"215824eb-db0e-4646-aa2a-c38337e820b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b57c9885-fb33-4fa6-8a07-13db581a067b","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b57c9885-fb33-4fa6-8a07-13db581a067b","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":{"name":"dfa6078b-0891-450a-b4d0-440a510fead7","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"b57c9885-fb33-4fa6-8a07-13db581a067b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8ed6ec7a-fef8-4578-8101-7ce6115467b2","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8ed6ec7a-fef8-4578-8101-7ce6115467b2","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":{"name":"dfa6078b-0891-450a-b4d0-440a510fead7","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"8ed6ec7a-fef8-4578-8101-7ce6115467b2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7e773721-3e53-4912-85a5-d69192c22b11","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7e773721-3e53-4912-85a5-d69192c22b11","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":{"name":"dfa6078b-0891-450a-b4d0-440a510fead7","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"7e773721-3e53-4912-85a5-d69192c22b11","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d86a2dc1-17cd-41a0-bc80-b2674e1a8caf","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d86a2dc1-17cd-41a0-bc80-b2674e1a8caf","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"LayerId":{"name":"dfa6078b-0891-450a-b4d0-440a510fead7","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","name":"d86a2dc1-17cd-41a0-bc80-b2674e1a8caf","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"289cc49a-eed6-4140-acdc-9eeaac6fc824","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f2eaf341-b4c1-4094-9006-c1c16c7dd76f","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"44047b2b-3e90-47ea-8db6-927e6cd9431b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3224d7c9-2900-40ee-bd9b-3c217583f806","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0f6f6256-51f7-4e31-84b1-b4fe27b5d4f1","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"782e59b4-3a43-48ca-aa87-c9913b68542c","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f7649f6e-3e10-48dd-b13a-ded2ccbe2f35","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"215824eb-db0e-4646-aa2a-c38337e820b5","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"27013e2c-17dd-462b-8d86-d9938c6f2f33","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b57c9885-fb33-4fa6-8a07-13db581a067b","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"146edc3f-4a6a-45cb-9c8d-d0292a5a9f26","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8ed6ec7a-fef8-4578-8101-7ce6115467b2","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"25bf9c04-015f-44f0-a1e3-3b25f816ab48","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7e773721-3e53-4912-85a5-d69192c22b11","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6eeebb6c-a4be-415a-ab4f-34f68acda3ec","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d86a2dc1-17cd-41a0-bc80-b2674e1a8caf","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 4,
    "yorigin": 5,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite_fire_ash_particle","path":"sprites/sprite_fire_ash_particle/sprite_fire_ash_particle.yy",},
    "resourceVersion": "1.3",
    "name": "sprite_fire_ash_particle",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"dfa6078b-0891-450a-b4d0-440a510fead7","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "particlesSprites",
    "path": "folders/PARTICLES/particlesSprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite_fire_ash_particle",
  "tags": [],
  "resourceType": "GMSprite",
}