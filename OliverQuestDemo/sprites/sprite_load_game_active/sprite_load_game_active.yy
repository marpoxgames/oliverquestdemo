{
  "bboxMode": 0,
  "collisionKind": 0,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 59,
  "bbox_top": 8,
  "bbox_bottom": 62,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"005dbac8-ae49-48d0-8404-dc08a9cec513","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"005dbac8-ae49-48d0-8404-dc08a9cec513","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":{"name":"8ae99580-da58-4886-ab80-b70ccd017ae4","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_load_game_active","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"005dbac8-ae49-48d0-8404-dc08a9cec513","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a61fae78-f584-4e7d-9722-6dff25fe4307","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a61fae78-f584-4e7d-9722-6dff25fe4307","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":{"name":"8ae99580-da58-4886-ab80-b70ccd017ae4","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_load_game_active","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"a61fae78-f584-4e7d-9722-6dff25fe4307","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0b4499bb-888a-4879-99bc-8c68c637b6f4","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0b4499bb-888a-4879-99bc-8c68c637b6f4","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":{"name":"8ae99580-da58-4886-ab80-b70ccd017ae4","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_load_game_active","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"0b4499bb-888a-4879-99bc-8c68c637b6f4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"31ee83e4-4a8a-44f3-8d05-4e29306142e9","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"31ee83e4-4a8a-44f3-8d05-4e29306142e9","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":{"name":"8ae99580-da58-4886-ab80-b70ccd017ae4","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_load_game_active","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"31ee83e4-4a8a-44f3-8d05-4e29306142e9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d3722e45-d6c1-4512-b03d-d19c1777cb7b","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d3722e45-d6c1-4512-b03d-d19c1777cb7b","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":{"name":"8ae99580-da58-4886-ab80-b70ccd017ae4","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_load_game_active","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"d3722e45-d6c1-4512-b03d-d19c1777cb7b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bebf0c2b-86ba-4a27-9e92-ba0ca7e1086f","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bebf0c2b-86ba-4a27-9e92-ba0ca7e1086f","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"LayerId":{"name":"8ae99580-da58-4886-ab80-b70ccd017ae4","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_load_game_active","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","name":"bebf0c2b-86ba-4a27-9e92-ba0ca7e1086f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite_load_game_active","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6ff9f793-79eb-4720-bcd3-cee4690a838c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"005dbac8-ae49-48d0-8404-dc08a9cec513","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eaba2895-c69b-4695-bd69-eb526811a4bc","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a61fae78-f584-4e7d-9722-6dff25fe4307","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"39390753-911e-46d2-8551-7ca04e1ff474","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0b4499bb-888a-4879-99bc-8c68c637b6f4","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5f530ed5-1f5b-4600-a504-9a0392163ee0","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"31ee83e4-4a8a-44f3-8d05-4e29306142e9","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b2aefd83-c63e-4083-be1f-70fc414d2d6a","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d3722e45-d6c1-4512-b03d-d19c1777cb7b","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"19302026-dcfe-4385-84fa-cd04bb90abce","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bebf0c2b-86ba-4a27-9e92-ba0ca7e1086f","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 62,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite_load_game_active","path":"sprites/sprite_load_game_active/sprite_load_game_active.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8ae99580-da58-4886-ab80-b70ccd017ae4","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "START MENU ",
    "path": "folders/Sprites/SPRITES/HUD/START MENU .yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite_load_game_active",
  "tags": [],
  "resourceType": "GMSprite",
}