{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 241,
  "bbox_top": 0,
  "bbox_bottom": 115,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f99013b1-6a48-4f8a-a174-730f10b9a404","path":"sprites/sprite_logo_menu_start/sprite_logo_menu_start.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f99013b1-6a48-4f8a-a174-730f10b9a404","path":"sprites/sprite_logo_menu_start/sprite_logo_menu_start.yy",},"LayerId":{"name":"0bc093d0-f22f-4349-ba0f-59487c97887b","path":"sprites/sprite_logo_menu_start/sprite_logo_menu_start.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_logo_menu_start","path":"sprites/sprite_logo_menu_start/sprite_logo_menu_start.yy",},"resourceVersion":"1.0","name":"f99013b1-6a48-4f8a-a174-730f10b9a404","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite_logo_menu_start","path":"sprites/sprite_logo_menu_start/sprite_logo_menu_start.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2e65d180-7ca9-4b04-b4ac-0482c684e782","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f99013b1-6a48-4f8a-a174-730f10b9a404","path":"sprites/sprite_logo_menu_start/sprite_logo_menu_start.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 128,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite_logo_menu_start","path":"sprites/sprite_logo_menu_start/sprite_logo_menu_start.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"0bc093d0-f22f-4349-ba0f-59487c97887b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "START MENU ",
    "path": "folders/ASSETs/Sprites/TILES/START MENU .yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite_logo_menu_start",
  "tags": [],
  "resourceType": "GMSprite",
}