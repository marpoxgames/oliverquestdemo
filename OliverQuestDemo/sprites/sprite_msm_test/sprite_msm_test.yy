{
  "bboxMode": 1,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 383,
  "bbox_top": 0,
  "bbox_bottom": 191,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 384,
  "height": 192,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"252d6e50-b8d3-41b1-bd0f-6ce5b5201fda","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"252d6e50-b8d3-41b1-bd0f-6ce5b5201fda","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":{"name":"972da3d6-c0ed-45cc-8057-2de01e5ca628","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_msm_test","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"252d6e50-b8d3-41b1-bd0f-6ce5b5201fda","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"42a03fb8-98e9-4791-bef8-f353e86a10d8","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"42a03fb8-98e9-4791-bef8-f353e86a10d8","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":{"name":"972da3d6-c0ed-45cc-8057-2de01e5ca628","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_msm_test","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"42a03fb8-98e9-4791-bef8-f353e86a10d8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"615babb9-c23a-4fd8-ace7-dbf9b336213a","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"615babb9-c23a-4fd8-ace7-dbf9b336213a","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":{"name":"972da3d6-c0ed-45cc-8057-2de01e5ca628","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_msm_test","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"615babb9-c23a-4fd8-ace7-dbf9b336213a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"15d8673a-cd55-47f9-8341-e7589b46c699","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"15d8673a-cd55-47f9-8341-e7589b46c699","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":{"name":"972da3d6-c0ed-45cc-8057-2de01e5ca628","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_msm_test","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"15d8673a-cd55-47f9-8341-e7589b46c699","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"203a3b0d-57d6-4efa-a13e-93c7ca2d824a","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"203a3b0d-57d6-4efa-a13e-93c7ca2d824a","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"LayerId":{"name":"972da3d6-c0ed-45cc-8057-2de01e5ca628","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_msm_test","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","name":"203a3b0d-57d6-4efa-a13e-93c7ca2d824a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite_msm_test","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2f6e1d37-bc52-4a5c-bfd1-5aab647d5fec","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"252d6e50-b8d3-41b1-bd0f-6ce5b5201fda","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"89076ac7-40cf-419f-8960-cb964945349a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"42a03fb8-98e9-4791-bef8-f353e86a10d8","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a8bf3473-78f0-4b57-8eb9-640deab7c2a9","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"615babb9-c23a-4fd8-ace7-dbf9b336213a","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4ee619c4-01ac-466f-a5a1-b81df9a99b2a","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"15d8673a-cd55-47f9-8341-e7589b46c699","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9eb2ed67-c92d-4033-9699-5b8e84e3d63c","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"203a3b0d-57d6-4efa-a13e-93c7ca2d824a","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 192,
    "yorigin": 96,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite_msm_test","path":"sprites/sprite_msm_test/sprite_msm_test.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"972da3d6-c0ed-45cc-8057-2de01e5ca628","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "TEXT BOXES",
    "path": "folders/Sprites/SPRITES/HUD/TEXT BOXES.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite_msm_test",
  "tags": [],
  "resourceType": "GMSprite",
}