{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 15,
  "bbox_right": 47,
  "bbox_top": 6,
  "bbox_bottom": 62,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"dbc2d69b-bfeb-440a-9f07-09be2e2b487c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dbc2d69b-bfeb-440a-9f07-09be2e2b487c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":{"name":"e9e1a2ac-5e72-4b29-ac73-6628a159c57c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_npc_kit_to_dead","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"dbc2d69b-bfeb-440a-9f07-09be2e2b487c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"965e77a3-364e-48f4-8f1d-78bea7452894","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"965e77a3-364e-48f4-8f1d-78bea7452894","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":{"name":"e9e1a2ac-5e72-4b29-ac73-6628a159c57c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_npc_kit_to_dead","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"965e77a3-364e-48f4-8f1d-78bea7452894","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"71c850fb-3280-4c66-a8dd-eb89eec1ba2d","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"71c850fb-3280-4c66-a8dd-eb89eec1ba2d","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":{"name":"e9e1a2ac-5e72-4b29-ac73-6628a159c57c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_npc_kit_to_dead","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"71c850fb-3280-4c66-a8dd-eb89eec1ba2d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ebff3e22-80ed-4910-acdc-2e418016e364","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ebff3e22-80ed-4910-acdc-2e418016e364","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":{"name":"e9e1a2ac-5e72-4b29-ac73-6628a159c57c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_npc_kit_to_dead","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"ebff3e22-80ed-4910-acdc-2e418016e364","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"249796b1-dbea-4307-a0ca-150dc583607c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"249796b1-dbea-4307-a0ca-150dc583607c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":{"name":"e9e1a2ac-5e72-4b29-ac73-6628a159c57c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_npc_kit_to_dead","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"249796b1-dbea-4307-a0ca-150dc583607c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0b70945d-5bc8-446c-aca2-5c26116be661","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0b70945d-5bc8-446c-aca2-5c26116be661","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"LayerId":{"name":"e9e1a2ac-5e72-4b29-ac73-6628a159c57c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_npc_kit_to_dead","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","name":"0b70945d-5bc8-446c-aca2-5c26116be661","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite_npc_kit_to_dead","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9260688f-915c-4ebd-b9e8-81c5b6e3752b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dbc2d69b-bfeb-440a-9f07-09be2e2b487c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63ac1a5e-25bb-4771-ab8f-e3a0beb6fc12","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"965e77a3-364e-48f4-8f1d-78bea7452894","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"09e691bf-0709-4570-98ba-a15833c514ed","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"71c850fb-3280-4c66-a8dd-eb89eec1ba2d","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8561dd10-93cb-4b10-adb7-fb232f8605d7","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ebff3e22-80ed-4910-acdc-2e418016e364","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"91f40bbb-db62-4bdb-9e08-9de193ccb637","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"249796b1-dbea-4307-a0ca-150dc583607c","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"444e87f8-8330-4087-9492-50906663211f","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0b70945d-5bc8-446c-aca2-5c26116be661","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 63,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite_npc_kit_to_dead","path":"sprites/sprite_npc_kit_to_dead/sprite_npc_kit_to_dead.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e9e1a2ac-5e72-4b29-ac73-6628a159c57c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "DEAD",
    "path": "folders/Sprites/SPRITES/SABIOS/KIT/CARACTER/DEAD.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite_npc_kit_to_dead",
  "tags": [],
  "resourceType": "GMSprite",
}