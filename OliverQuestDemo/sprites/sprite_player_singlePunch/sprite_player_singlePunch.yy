{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 26,
  "bbox_right": 38,
  "bbox_top": 9,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"24d84548-e595-4639-8461-efeb9af4423b","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"24d84548-e595-4639-8461-efeb9af4423b","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"24d84548-e595-4639-8461-efeb9af4423b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"74f27a08-556b-44d4-86ac-c696e343fd48","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"74f27a08-556b-44d4-86ac-c696e343fd48","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"74f27a08-556b-44d4-86ac-c696e343fd48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2af190be-c547-4b1a-bfb6-9672c3771bcf","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2af190be-c547-4b1a-bfb6-9672c3771bcf","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"2af190be-c547-4b1a-bfb6-9672c3771bcf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4179554e-d7ca-412b-a14f-7a65f302f831","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4179554e-d7ca-412b-a14f-7a65f302f831","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"4179554e-d7ca-412b-a14f-7a65f302f831","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e357d15e-b8ba-475f-9bb7-5b88ab25d5c3","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e357d15e-b8ba-475f-9bb7-5b88ab25d5c3","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"e357d15e-b8ba-475f-9bb7-5b88ab25d5c3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"50b347bc-2041-462a-9f86-302e826c4b2a","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"50b347bc-2041-462a-9f86-302e826c4b2a","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"50b347bc-2041-462a-9f86-302e826c4b2a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a64b3e81-1416-42a8-ab50-8dacb57e8dcb","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a64b3e81-1416-42a8-ab50-8dacb57e8dcb","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"a64b3e81-1416-42a8-ab50-8dacb57e8dcb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ad6f354e-bafe-47f3-9c4f-05d23baf6321","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ad6f354e-bafe-47f3-9c4f-05d23baf6321","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"ad6f354e-bafe-47f3-9c4f-05d23baf6321","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"07455459-bbc6-4345-9a77-41ac9890f189","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"07455459-bbc6-4345-9a77-41ac9890f189","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"07455459-bbc6-4345-9a77-41ac9890f189","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"276a5a1d-c6fb-4da4-b7ef-395bfbeaf6a6","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"276a5a1d-c6fb-4da4-b7ef-395bfbeaf6a6","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"LayerId":{"name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","name":"276a5a1d-c6fb-4da4-b7ef-395bfbeaf6a6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 10.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"8ef70fc3-02a2-445f-af19-9986eda98e3b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"24d84548-e595-4639-8461-efeb9af4423b","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a82fa34d-d7cc-4baf-8b98-9fa03af27fdf","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"74f27a08-556b-44d4-86ac-c696e343fd48","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"af7a846a-3d51-47b2-8411-b43700447c07","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2af190be-c547-4b1a-bfb6-9672c3771bcf","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9832c1c6-1838-4dd2-a150-40ecd6e3d18d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4179554e-d7ca-412b-a14f-7a65f302f831","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63acc5a4-d9ee-46af-903e-8161ac46db54","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e357d15e-b8ba-475f-9bb7-5b88ab25d5c3","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8053c1fa-639c-44f6-84e3-ad9d214b4058","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"50b347bc-2041-462a-9f86-302e826c4b2a","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"18c3df2f-87bb-4e25-8d4b-c6fbb3ac42d4","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a64b3e81-1416-42a8-ab50-8dacb57e8dcb","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7aacc053-2f9a-4701-9756-830441248aec","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ad6f354e-bafe-47f3-9c4f-05d23baf6321","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"03bb3ad2-26a9-4006-9d41-0d68262c0e6c","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"07455459-bbc6-4345-9a77-41ac9890f189","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"90c9c19e-88e2-4739-8a1e-7d06b7862c82","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"276a5a1d-c6fb-4da4-b7ef-395bfbeaf6a6","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite_player_singlePunch","path":"sprites/sprite_player_singlePunch/sprite_player_singlePunch.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f06c4424-f685-4f2a-a0ab-74b9d3fe390d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "PLAYER",
    "path": "folders/Sprites/SPRITES/PLAYER.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite_player_singlePunch",
  "tags": [],
  "resourceType": "GMSprite",
}