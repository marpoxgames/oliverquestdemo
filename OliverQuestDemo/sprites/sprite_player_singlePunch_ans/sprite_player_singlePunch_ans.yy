{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 17,
  "bbox_right": 46,
  "bbox_top": 9,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"bcb9b6af-44df-4d67-a12a-a1c36534ad4c","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bcb9b6af-44df-4d67-a12a-a1c36534ad4c","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"bcb9b6af-44df-4d67-a12a-a1c36534ad4c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"094c9c2f-de50-4d8b-9ccc-a414d9bf56cd","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"094c9c2f-de50-4d8b-9ccc-a414d9bf56cd","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"094c9c2f-de50-4d8b-9ccc-a414d9bf56cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4c4bfbde-4986-4cde-922c-5c7de6bd6fed","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4c4bfbde-4986-4cde-922c-5c7de6bd6fed","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"4c4bfbde-4986-4cde-922c-5c7de6bd6fed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"17fd9699-0bd1-40a6-9acc-9a3eecffafee","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"17fd9699-0bd1-40a6-9acc-9a3eecffafee","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"17fd9699-0bd1-40a6-9acc-9a3eecffafee","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bf65b618-a4b4-40e7-bbe6-c77e50e45cad","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bf65b618-a4b4-40e7-bbe6-c77e50e45cad","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"bf65b618-a4b4-40e7-bbe6-c77e50e45cad","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"66dd8426-6753-4214-847e-5d7fd0911b9b","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"66dd8426-6753-4214-847e-5d7fd0911b9b","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"66dd8426-6753-4214-847e-5d7fd0911b9b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1b795a83-e43c-4fee-a67f-b81595ccb50b","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1b795a83-e43c-4fee-a67f-b81595ccb50b","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"1b795a83-e43c-4fee-a67f-b81595ccb50b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9ba9c3c1-1b45-4236-ac66-2a1fd8ce5c4e","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9ba9c3c1-1b45-4236-ac66-2a1fd8ce5c4e","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"9ba9c3c1-1b45-4236-ac66-2a1fd8ce5c4e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f1b34dae-8a00-49ea-93ae-cbb8986bd417","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f1b34dae-8a00-49ea-93ae-cbb8986bd417","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"f1b34dae-8a00-49ea-93ae-cbb8986bd417","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d7630da1-6e13-4535-aee8-6305c2c222c0","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d7630da1-6e13-4535-aee8-6305c2c222c0","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"LayerId":{"name":"8a28c8d1-efd5-42e2-b130-b6d828940443","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","name":"d7630da1-6e13-4535-aee8-6305c2c222c0","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 10.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4380368f-6ecf-48ca-9581-01c3b5dddb89","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bcb9b6af-44df-4d67-a12a-a1c36534ad4c","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9d8a466c-e828-4f19-9241-67bcd11cba91","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"094c9c2f-de50-4d8b-9ccc-a414d9bf56cd","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"af180854-991d-4cc1-bb3f-e6752c08a8cd","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4c4bfbde-4986-4cde-922c-5c7de6bd6fed","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cda4ace6-4b0f-4fb8-b0e5-7f5cf0c254d7","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"17fd9699-0bd1-40a6-9acc-9a3eecffafee","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9a6226e3-3848-46d1-bc74-825c99aa728a","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf65b618-a4b4-40e7-bbe6-c77e50e45cad","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"386eb7af-03f4-47fa-8edf-e2cc9a85dcd8","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"66dd8426-6753-4214-847e-5d7fd0911b9b","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"614f4ba2-548d-43a8-9c4d-64c33fae0e0c","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1b795a83-e43c-4fee-a67f-b81595ccb50b","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f77047ca-8ee2-404f-be03-519b41b119fd","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9ba9c3c1-1b45-4236-ac66-2a1fd8ce5c4e","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f1b44371-9d5a-4a76-a2a4-6a368be3028c","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f1b34dae-8a00-49ea-93ae-cbb8986bd417","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d7f39db6-18a5-4cfa-81da-7383df6969b4","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d7630da1-6e13-4535-aee8-6305c2c222c0","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite_player_singlePunch_ans","path":"sprites/sprite_player_singlePunch_ans/sprite_player_singlePunch_ans.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8a28c8d1-efd5-42e2-b130-b6d828940443","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "ANSWER_GET",
    "path": "folders/Sprites/SPRITES/PLAYER/NORMAL/ANSWER_GET.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite_player_singlePunch_ans",
  "tags": [],
  "resourceType": "GMSprite",
}