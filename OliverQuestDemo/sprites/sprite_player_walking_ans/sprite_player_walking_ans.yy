{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 17,
  "bbox_right": 46,
  "bbox_top": 9,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"20f1e958-3d25-44bb-9837-9e602f1b1735","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"20f1e958-3d25-44bb-9837-9e602f1b1735","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":{"name":"39a1aff6-1697-413d-a655-8136d9696e4b","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_walking_ans","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"20f1e958-3d25-44bb-9837-9e602f1b1735","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"57ab7749-0d1d-494e-bc61-9a36f86e54d1","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"57ab7749-0d1d-494e-bc61-9a36f86e54d1","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":{"name":"39a1aff6-1697-413d-a655-8136d9696e4b","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_walking_ans","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"57ab7749-0d1d-494e-bc61-9a36f86e54d1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9b72a068-2d48-4a7c-8558-1417dd9274b1","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9b72a068-2d48-4a7c-8558-1417dd9274b1","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":{"name":"39a1aff6-1697-413d-a655-8136d9696e4b","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_walking_ans","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"9b72a068-2d48-4a7c-8558-1417dd9274b1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d219dde0-c402-44e5-8558-af2c1589204d","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d219dde0-c402-44e5-8558-af2c1589204d","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":{"name":"39a1aff6-1697-413d-a655-8136d9696e4b","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_walking_ans","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"d219dde0-c402-44e5-8558-af2c1589204d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d6a72885-efcf-44ff-a666-beb653b70bd1","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d6a72885-efcf-44ff-a666-beb653b70bd1","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":{"name":"39a1aff6-1697-413d-a655-8136d9696e4b","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_walking_ans","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"d6a72885-efcf-44ff-a666-beb653b70bd1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"20ca1be3-6d9e-404e-9130-cec2351189c4","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"20ca1be3-6d9e-404e-9130-cec2351189c4","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"LayerId":{"name":"39a1aff6-1697-413d-a655-8136d9696e4b","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite_player_walking_ans","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","name":"20ca1be3-6d9e-404e-9130-cec2351189c4","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite_player_walking_ans","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4c9a72d1-75c9-44a6-bd8a-77b1a76aa30f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"20f1e958-3d25-44bb-9837-9e602f1b1735","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"826e0f09-e8a7-4d30-9273-2f50cc347a25","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"57ab7749-0d1d-494e-bc61-9a36f86e54d1","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"337025d9-459a-4fb1-a924-a4eff22840ef","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9b72a068-2d48-4a7c-8558-1417dd9274b1","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"53c246fe-7392-4e12-8094-aaa7d961cbb8","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d219dde0-c402-44e5-8558-af2c1589204d","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cc894648-834c-42f2-b5a0-2a0830abf795","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d6a72885-efcf-44ff-a666-beb653b70bd1","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"781a04af-f325-4638-bf39-609967ea3965","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"20ca1be3-6d9e-404e-9130-cec2351189c4","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite_player_walking_ans","path":"sprites/sprite_player_walking_ans/sprite_player_walking_ans.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"39a1aff6-1697-413d-a655-8136d9696e4b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "ANSWER_GET",
    "path": "folders/Sprites/SPRITES/PLAYER/NORMAL/ANSWER_GET.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite_player_walking_ans",
  "tags": [],
  "resourceType": "GMSprite",
}